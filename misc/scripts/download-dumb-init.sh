#!/usr/bin/sh

VERSION='1.2.5'
FILENAME="${1:-dumb-init}"

if [ "$TARGETARCH" = "arm64" ]; then
  wget -O "${FILENAME}" "https://github.com/Yelp/dumb-init/releases/download/v${VERSION}/dumb-init_${VERSION}_aarch64"
else
  wget -O "${FILENAME}" "https://github.com/Yelp/dumb-init/releases/download/v${VERSION}/dumb-init_${VERSION}_x86_64"
fi

chmod +x "$FILENAME"
