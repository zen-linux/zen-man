export abstract class SettingsService {
  // general
  abstract getDbConnectionString(): string;
  abstract getJwtSecret(): string;
  abstract getDataPath(): string;

  // s3
  abstract getS3AccessKey(): string;
  abstract getS3SecrectKey(): string;
  abstract getS3Endpoint(): string;
  abstract getS3Region(): string;
  abstract getS3AssetsBucket(): string;
  abstract getS3ReposBucket(): string;

  // mail
  abstract getSenderEmail(): string;
  abstract getSendgridApiKey(): string;

  // redis
  abstract getRedisHost(): string;
  abstract getRedisPort(): number;
  abstract getRedisUsername(): string;
  abstract getRedisPassword(): string;

  abstract getAssetsUrl(): string;
  abstract getReposUrl(): string;
}
