export abstract class TemplateService {
  abstract render(templatePath: string, context?: object): Promise<string>;
}
