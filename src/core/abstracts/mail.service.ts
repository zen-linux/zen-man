import { Mail } from '@core/models/mail';
import { SentMailInfo } from '@core/models/sent-mail-info';

export abstract class MailService {
  abstract send(mail: Mail): Promise<SentMailInfo>;
}
