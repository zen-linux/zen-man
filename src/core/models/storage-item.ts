export interface StorageItem {
  path: string;
  size: number;
  date: Date;
  etag: string;
}
