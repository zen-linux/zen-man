export type MulterFile = Express.Multer.File & MulterFileAddons;

export type MulterFileAddons = {
  md5: string;
  sha256: string;
};
