export interface StorageFile {
  name: string;
  path: string;
  size: number;

  mimeType: string;

  pkgMetaData?: {
    desc: string;
  };
}
