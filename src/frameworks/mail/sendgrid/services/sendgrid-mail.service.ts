import { MailService } from '@core/abstracts/mail.service';
import { SettingsService } from '@core/abstracts/settings.service';
import { Mail } from '@core/models/mail';
import { SentMailInfo } from '@core/models/sent-mail-info';
import { Injectable } from '@nestjs/common';
import {
  MailDataRequired,
  MailService as SendgridgMailService,
} from '@sendgrid/mail';

@Injectable()
export class SendgridMailService implements MailService {
  private mailService: SendgridgMailService;
  private senderEmail: string;

  constructor(readonly settings: SettingsService) {
    this.mailService = new SendgridgMailService();
    this.mailService.setApiKey(settings.getSendgridApiKey());

    this.senderEmail = settings.getSenderEmail();
  }

  async send(mail: Mail): Promise<SentMailInfo> {
    const mailData: MailDataRequired = {
      to: mail.to,
      from: this.senderEmail,
      subject: mail.subject,
      text: mail.text,
      html: mail.html,
    };

    ////console.log('sendgrid mail:', mailData);
    const [clientResponse] = await this.mailService.send(mailData);
    ////console.log('sendgrid response:', clientResponse);

    const sentMailInfo: SentMailInfo = {
      id: clientResponse.headers['x-message-id'],
    };

    return sentMailInfo;
  }
}
