import { SettingsModule } from '@modules/settings/settings.module';
import { Module } from '@nestjs/common';
import { SendgridMailService } from './services/sendgrid-mail.service';

@Module({
  imports: [SettingsModule],
  providers: [SendgridMailService],
  exports: [SendgridMailService],
})
export class SendgridMailModule {}
