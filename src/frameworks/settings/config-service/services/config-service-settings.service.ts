import { SettingsService } from '@core/abstracts/settings.service';
import { Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { EnvironmentVariables } from '../environment-variables';

@Injectable()
export class ConfigServiceSettingsService implements SettingsService {
  constructor(
    private configService: ConfigService<EnvironmentVariables, true>,
  ) {}

  getDataPath(): string {
    return this.configService.get('DATA_PATH');
  }

  getDbConnectionString(): string {
    return this.configService.get('DB_CONNECTION_STRING');
  }

  getJwtSecret(): string {
    return this.configService.get('JWT_SECRET');
  }

  // s3

  getS3AccessKey(): string {
    return this.configService.get('S3_ACCESS_KEY');
  }

  getS3SecrectKey(): string {
    return this.configService.get('S3_SECRET_KEY');
  }

  getS3Endpoint(): string {
    return this.configService.get('S3_ENDPOINT');
  }

  getS3Region(): string {
    return this.configService.get('S3_REGION');
  }

  getS3AssetsBucket(): string {
    return this.configService.get('S3_BUCKET_ASSETS');
  }

  getS3ReposBucket(): string {
    return this.configService.get('S3_BUCKET_REPOS');
  }

  // mail

  getSenderEmail(): string {
    return this.configService.get('SENDER_EMAIL', 'info@zenlinux.net');
  }

  getSendgridApiKey(): string {
    return this.configService.get('SENDGRID_API_KEY');
  }

  // redis

  getRedisHost(): string {
    return this.configService.get('REDIS_HOST', 'localhost');
  }

  getRedisPort(): number {
    return this.configService.get('REDIS_PORT', 6379);
  }

  getRedisUsername(): string {
    return this.configService.get('REDIS_USERNAME', '');
  }

  getRedisPassword(): string {
    return this.configService.get('REDIS_PASSWORD', '');
  }

  // public urls

  getAssetsUrl(): string {
    return this.configService.get('ASSETS_URL');
  }

  getReposUrl(): string {
    return this.configService.get('REPOS_URL');
  }
}
