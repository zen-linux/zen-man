import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { validate } from './environment-variables';
import { ConfigServiceSettingsService } from './services/config-service-settings.service';

@Module({
  imports: [
    ConfigModule.forRoot({
      validate,
    }),
  ],
  providers: [ConfigServiceSettingsService],
  exports: [ConfigServiceSettingsService],
})
export class ConfigServiceSettingsModule {}
