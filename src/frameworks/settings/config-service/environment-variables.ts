import { plainToInstance } from 'class-transformer';
import {
  IsNumber,
  IsOptional,
  IsString,
  MinLength,
  validateSync,
} from 'class-validator';

export class EnvironmentVariables {
  @IsString()
  DB_CONNECTION_STRING!: string;

  @IsString()
  @MinLength(32)
  JWT_SECRET!: string;

  @IsString()
  DATA_PATH!: string;

  @IsString()
  S3_ACCESS_KEY!: string;

  @IsString()
  S3_SECRET_KEY!: string;

  @IsString()
  S3_ENDPOINT!: string;

  @IsString()
  S3_REGION!: string;

  @IsString()
  S3_BUCKET_ASSETS!: string;

  @IsString()
  S3_BUCKET_REPOS!: string;

  // mail

  @IsString()
  @IsOptional()
  SENDER_EMAIL!: string;

  @IsString()
  SENDGRID_API_KEY!: string;

  // redis

  @IsString()
  REDIS_HOST!: string;

  @IsNumber()
  REDIS_PORT!: number;

  @IsString()
  REDIS_USERNAME!: string;

  @IsString()
  REDIS_PASSWORD!: string;

  // public urls

  @IsString()
  ASSETS_URL!: string;

  @IsString()
  REPOS_URL!: string;
}

export function validate(
  config: Record<string, unknown>,
): EnvironmentVariables {
  const validatedConfig = plainToInstance(EnvironmentVariables, config, {
    enableImplicitConversion: true,
  });
  const errors = validateSync(validatedConfig, {
    skipMissingProperties: false,
  });

  if (errors.length > 0) {
    throw new Error(errors.toString());
  }
  return validatedConfig;
}
