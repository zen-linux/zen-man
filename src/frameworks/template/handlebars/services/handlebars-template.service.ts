import { TemplateService } from '@core/abstracts/template.service';
import { Injectable } from '@nestjs/common';
import { readFile } from 'fs/promises';
import { compile } from 'handlebars';
import { resolve } from 'path';

@Injectable()
export class HandlebarsTemplateService implements TemplateService {
  async render(templatePath: string, context?: object): Promise<string> {
    const template = (
      await readFile(
        resolve(
          __dirname,
          '..',
          '..',
          '..',
          '..',
          'assets',
          'templates',
          templatePath,
        ),
      )
    ).toString();

    const compiledTemplate = compile(template);
    const renderedTempate = compiledTemplate(context);

    return renderedTempate;
  }
}
