import { Module } from '@nestjs/common';
import { HandlebarsTemplateService } from './services/handlebars-template.service';

@Module({
  providers: [HandlebarsTemplateService],
  exports: [HandlebarsTemplateService],
})
export class HandlebarsTemplateModule {}
