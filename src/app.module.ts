import { SettingsService } from '@core/abstracts/settings.service';
import { ApiKeyModule } from '@modules/api-key/api-key.module';
import { ArchModule } from '@modules/arch/arch.module';
import { AuthModule } from '@modules/auth/auth.module';
import { DataModule } from '@modules/data/data.module';
import { GroupModule } from '@modules/group/group.module';
import { IsoModule } from '@modules/iso/iso.module';
import { RepoModule } from '@modules/repo/repo.module';
import { SettingsModule } from '@modules/settings/settings.module';
import { UserModule } from '@modules/user/user.module';
import { BullModule } from '@nestjs/bull';
import { MiddlewareConsumer, Module, NestModule } from '@nestjs/common';
import { RequestLoggerMiddleware } from './core/middlewares/request-logger.middleware';

@Module({
  imports: [
    AuthModule,
    ApiKeyModule,
    ArchModule,
    BullModule.forRootAsync({
      imports: [SettingsModule],
      useFactory: (settings: SettingsService) => ({
        redis: {
          host: settings.getRedisHost(),
          port: settings.getRedisPort(),
          username: settings.getRedisUsername(),
          password: settings.getRedisPassword(),
        },
        prefix: process.env.NODE_ENV === 'production' ? 'live' : 'test',
        defaultJobOptions: {
          removeOnComplete: 3,
          removeOnFail: 3,
          attempts: 1,
          backoff: {
            type: 'exponential',
            delay: 10000,
          },
        },
      }),
      inject: [SettingsService],
    }),
    DataModule,
    GroupModule,
    IsoModule,
    RepoModule,
    UserModule,
  ],
})
export class AppModule implements NestModule {
  configure(consumer: MiddlewareConsumer) {
    consumer.apply(RequestLoggerMiddleware).forRoutes('*');
  }
}
