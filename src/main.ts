import { PrismaErrorInterceptor } from '@core/interceptors/prisma-error.interceptor';
import { PrismaService } from '@modules/data/services/prisma.service';
import { Logger, ValidationPipe } from '@nestjs/common';
import { NestFactory } from '@nestjs/core';
import { NestExpressApplication } from '@nestjs/platform-express';
import { useContainer } from 'class-validator';
import * as cookieParser from 'cookie-parser';
import helmet from 'helmet';
import { AppModule } from './app.module';

export const API_PREFIX = 'api';

async function bootstrap() {
  const app = await NestFactory.create<NestExpressApplication>(AppModule, {
    logger: ['error', 'warn', 'log', 'debug', 'verbose'],
  });
  app.setGlobalPrefix(API_PREFIX);
  app.use(
    helmet({
      contentSecurityPolicy:
        process.env.NODE_ENV === 'production' ? undefined : false,
    }),
    cookieParser(),
  );
  app.enableCors({
    origin: ['https://zenlinux.net'],
  });
  app.useGlobalPipes(
    new ValidationPipe({
      whitelist: true,
      transform: true,
    }),
  );
  app.useGlobalInterceptors(new PrismaErrorInterceptor());
  app.enableVersioning();
  useContainer(app.select(AppModule), { fallbackOnErrors: true });

  const prismaService = app.get(PrismaService);
  await prismaService.enableShutdownHooks(app);

  const port = 3000;
  await app.listen(port);
  Logger.log(
    `🚀 Application is running on: http://localhost:${port}/${API_PREFIX}`,
  );
}

bootstrap();
