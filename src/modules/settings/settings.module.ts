import { SettingsService } from '@core/abstracts/settings.service';
import { ConfigServiceSettingsModule } from '@frameworks/settings/config-service/config-service-settings.module';
import { ConfigServiceSettingsService } from '@frameworks/settings/config-service/services/config-service-settings.service';
import { Module } from '@nestjs/common';

@Module({
  imports: [ConfigServiceSettingsModule],
  providers: [
    {
      provide: SettingsService,
      useExisting: ConfigServiceSettingsService,
    },
  ],
  exports: [SettingsService],
})
export class SettingsModule {}
