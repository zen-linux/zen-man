import { MailService } from '@core/abstracts/mail.service';
import { SendgridMailModule } from '@frameworks/mail/sendgrid/sendgrid-mail.module';
import { SendgridMailService } from '@frameworks/mail/sendgrid/services/sendgrid-mail.service';
import { Module } from '@nestjs/common';

@Module({
  imports: [SendgridMailModule],
  providers: [
    {
      provide: MailService,
      useExisting: SendgridMailService,
    },
  ],
  exports: [MailService],
})
export class MailModule {}
