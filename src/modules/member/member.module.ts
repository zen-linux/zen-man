import { GroupModule } from '@modules/group/group.module';
import { UserModule } from '@modules/user/user.module';
import { Module } from '@nestjs/common';

@Module({
  imports: [GroupModule, UserModule],
  providers: [],
})
export class MemberModule {}
