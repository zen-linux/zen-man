import { IsBoolean, IsNotEmpty, IsString } from 'class-validator';

export class CreateMemberInput {
  @IsString()
  @IsNotEmpty()
  userId!: string;

  @IsString()
  @IsNotEmpty()
  groupId!: string;

  @IsBoolean()
  isAdmin!: boolean;
}

export class UpdateMemberInput {
  @IsBoolean()
  isAdmin: boolean;
}

export class Member {
  id!: string;

  userId!: string;

  groupId!: string;

  isAdmin!: boolean;

  createdAt!: Date;

  modifiedAt!: Date;
}
