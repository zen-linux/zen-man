import { Global, Module } from '@nestjs/common';
import { ApiKeyDataService } from './services/api-key-data.service';
import { ArchDataService } from './services/arch-data.service';
import { AuthDataService } from './services/auth-data.service';
import { GroupDataService } from './services/group-data.service';
import { IsoDataService } from './services/iso-data.service';
import { MemberDataService } from './services/member-data.service';
import { PkgDataService } from './services/pkg-data.service';
import { PkgDependDataService } from './services/pkg-depend-data.service';
import { PkgNameDataService } from './services/pkg-name-data.service';
import { PrismaService } from './services/prisma.service';
import { RepoDataService } from './services/repo-data.service';
import { UserDataService } from './services/user-data.service';

@Global()
@Module({
  providers: [
    PrismaService,
    ApiKeyDataService,
    ArchDataService,
    AuthDataService,
    GroupDataService,
    IsoDataService,
    MemberDataService,
    PkgDataService,
    PkgDependDataService,
    PkgNameDataService,
    RepoDataService,
    UserDataService,
  ],
  exports: [
    PrismaService,
    ApiKeyDataService,
    ArchDataService,
    AuthDataService,
    GroupDataService,
    IsoDataService,
    MemberDataService,
    PkgDataService,
    PkgDependDataService,
    PkgNameDataService,
    RepoDataService,
    UserDataService,
  ],
})
export class DataModule {}
