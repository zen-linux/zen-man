import { CreateMemberInput, Member } from '@modules/member/dtos/member.dto';
import {
  ForbiddenException,
  Injectable,
  UnprocessableEntityException,
} from '@nestjs/common';
import { PrismaService } from './prisma.service';

@Injectable()
export class MemberDataService {
  constructor(private readonly prismaService: PrismaService) {}

  async createMember(
    data: CreateMemberInput,
    userId?: string,
  ): Promise<Member> {
    if (userId) {
      const group = await this.prismaService.group.findUnique({
        where: { id: data.groupId },
      });
      if (!group) {
        throw new UnprocessableEntityException('invalid groupId');
      }
      const member = await this.prismaService.member.findFirst({
        where: { groupId: data.groupId, userId },
      });
      if (!member || !member.isAdmin) {
        throw new ForbiddenException('only group admins can add members');
      }
    }
    return this.prismaService.member.create({ data });
  }

  getMember(id: string, userId?: string): Promise<Member | null> {
    if (userId) {
      return this.prismaService.member.findFirst({
        where: {
          id,
          group: { members: { every: { userId } } },
        },
      });
    }
    return this.prismaService.member.findFirst({ where: { id } });
  }

  getMemberForGroupIdAndUserId(
    groupId: string,
    userId: string,
  ): Promise<Member | null> {
    return this.prismaService.member.findFirst({ where: { groupId, userId } });
  }

  getMembers(userId?: string): Promise<Member[]> {
    if (userId) {
      return this.prismaService.member.findMany({
        where: { group: { members: { some: { userId } } } },
      });
    }
    return this.prismaService.member.findMany();
  }

  getMembersForGroupId(groupId: string, userId?: string): Promise<Member[]> {
    if (userId) {
      return this.prismaService.member.findMany({
        where: {
          groupId,
          group: { members: { some: { userId } } },
        },
      });
    }
    return this.prismaService.member.findMany({ where: { groupId } });
  }
}
