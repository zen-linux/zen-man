import { Injectable, NotFoundException } from '@nestjs/common';
import { generateApiKey } from 'generate-api-key';
import {
  ApiKey,
  ApiKeyWithKey,
  CreateApiKeyInputInternal,
  UpdateApiKeyInput,
} from '../../api-key/dtos/api-key.dto';
import { DeleteInfo } from '../dtos/delete-info.dto';
import { PrismaService } from './prisma.service';
import { UserDataService } from './user-data.service';

@Injectable()
export class ApiKeyDataService {
  constructor(
    private readonly prismaService: PrismaService,
    private readonly userDataService: UserDataService,
  ) {}

  async createApiKey(
    data: CreateApiKeyInputInternal,
    userId?: string,
  ): Promise<ApiKeyWithKey> {
    if (userId) {
      const user = await this.userDataService.getUser(data.userId, userId);
      if (!user) {
        throw new NotFoundException();
      }
    }
    return this.prismaService.apiKey.create({
      data: {
        userId: data.userId,
        description: data.description,
        key: generateApiKey({ length: 32, prefix: 'zen' }) as string,
      },
    });
  }

  getApiKey(id: string, userId?: string): Promise<ApiKey | null> {
    if (userId) {
      return this.prismaService.apiKey.findFirst({
        select: {
          id: true,
          userId: true,
          description: true,
          lastUsed: true,
          createdAt: true,
          modifiedAt: true,
        },
        where: { id, userId },
      });
    }
    return this.prismaService.apiKey.findFirst({
      select: {
        id: true,
        userId: true,
        description: true,
        lastUsed: true,
        createdAt: true,
        modifiedAt: true,
      },
      where: { id },
    });
  }

  getApiKeys(userId?: string): Promise<ApiKey[]> {
    if (userId) {
      return this.prismaService.apiKey.findMany({
        select: {
          id: true,
          userId: true,
          description: true,
          lastUsed: true,
          createdAt: true,
          modifiedAt: true,
        },
        where: { userId },
        orderBy: { createdAt: 'desc' },
      });
    }
    return this.prismaService.apiKey.findMany({
      select: {
        id: true,
        userId: true,
        description: true,
        lastUsed: true,
        createdAt: true,
        modifiedAt: true,
      },
      orderBy: { createdAt: 'desc' },
    });
  }

  async updateApiKey(
    id: string,
    data: UpdateApiKeyInput,
    userId?: string,
  ): Promise<ApiKey> {
    if (userId) {
      const apiKey = await this.getApiKey(id, userId);
      if (!apiKey) {
        throw new NotFoundException();
      }
    }
    return this.prismaService.apiKey.update({
      select: {
        id: true,
        userId: true,
        description: true,
        lastUsed: true,
        createdAt: true,
        modifiedAt: true,
      },
      where: { id },
      data,
    });
  }

  async deleteApiKey(id: string, userId?: string): Promise<ApiKey> {
    if (userId) {
      const apiKey = await this.getApiKey(id, userId);
      if (!apiKey) {
        throw new NotFoundException();
      }
    }
    return this.prismaService.apiKey.delete({
      select: {
        id: true,
        userId: true,
        description: true,
        lastUsed: true,
        createdAt: true,
        modifiedAt: true,
      },
      where: { id },
    });
  }

  deleteApiKeys(userId?: string): Promise<DeleteInfo> {
    if (userId) {
      return this.prismaService.apiKey.deleteMany({ where: { userId } });
    }
    return this.prismaService.apiKey.deleteMany();
  }
}
