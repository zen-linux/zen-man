import { Injectable } from '@nestjs/common';
import { CreateUserInput, MyUser } from '../../user/dtos/user.dto';
import { PrismaService } from './prisma.service';

@Injectable()
export class UserDataService {
  constructor(private readonly prismaService: PrismaService) {}

  createUser(data: CreateUserInput): Promise<MyUser> {
    return this.prismaService.user.create({
      data: { ...data, isAdmin: false },
      select: {
        id: true,
        email: true,
        name: true,
        isAdmin: true,
        createdAt: true,
        modifiedAt: true,
      },
    });
  }

  getUser(id: string, userId?: string): Promise<MyUser | null> {
    if (userId) {
      return this.prismaService.user.findFirst({
        select: {
          id: true,
          email: true,
          name: true,
          isAdmin: true,
          createdAt: true,
          modifiedAt: true,
        },
        where: {
          id,
          members: {
            some: { group: { members: { some: { userId } } } },
          },
        },
      });
    }
    return this.prismaService.user.findFirst({
      select: {
        id: true,
        email: true,
        name: true,
        isAdmin: true,
        createdAt: true,
        modifiedAt: true,
      },
      where: { id },
    });
  }

  getUsers(userId?: string): Promise<MyUser[]> {
    if (userId) {
      return this.prismaService.user.findMany({
        select: {
          id: true,
          email: true,
          name: true,
          isAdmin: true,
          createdAt: true,
          modifiedAt: true,
        },
        where: {
          members: {
            some: { group: { members: { some: { userId } } } },
          },
        },
      });
    }
    return this.prismaService.user.findMany({
      select: {
        id: true,
        email: true,
        name: true,
        isAdmin: true,
        createdAt: true,
        modifiedAt: true,
      },
    });
  }

  getUserByEmail(email: string): Promise<MyUser | null> {
    return this.prismaService.user.findUnique({
      select: {
        id: true,
        email: true,
        name: true,
        isAdmin: true,
        createdAt: true,
        modifiedAt: true,
      },
      where: { email },
    });
  }
}
