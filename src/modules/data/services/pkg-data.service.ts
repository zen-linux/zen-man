import { CreatePkgInputInternal, Pkg } from '@modules/repo/dtos/pkg.dto';
import {
  Injectable,
  NotFoundException,
  UnprocessableEntityException,
} from '@nestjs/common';
import { DeleteInfo } from '../dtos/delete-info.dto';
import { PrismaService } from './prisma.service';

@Injectable()
export class PkgDataService {
  constructor(private readonly prismaService: PrismaService) {}

  getPkg(id: string, userId?: string): Promise<Pkg | null> {
    if (userId) {
      return this.prismaService.pkg.findFirst({
        where: {
          id,
          repo: { group: { members: { some: { userId } } } },
        },
      });
    }
    return this.prismaService.pkg.findFirst({ where: { id } });
  }

  getPkgByName(name: string, userId?: string): Promise<Pkg | null> {
    if (userId) {
      return this.prismaService.pkg.findFirst({
        where: {
          name,
          repo: { group: { members: { some: { userId } } } },
        },
      });
    }
    return this.prismaService.pkg.findFirst({ where: { name } });
  }

  getPkgByRepoIdAndPkgName(
    repoId: string,
    pkgName: string,
    userId?: string,
  ): Promise<Pkg | null> {
    if (userId) {
      return this.prismaService.pkg.findFirst({
        where: {
          repoId,
          name: pkgName,
          repo: { group: { members: { some: { userId } } } },
        },
      });
    }
    return this.prismaService.pkg.findFirst({
      where: { repoId, name: pkgName },
    });
  }

  getPkgByRepoIdAndPkgNameId(
    repoId: string,
    pkgNameId: string,
    userId?: string,
  ): Promise<Pkg | null> {
    if (userId) {
      return this.prismaService.pkg.findFirst({
        where: {
          repoId,
          pkgNameId,
          repo: { group: { members: { some: { userId } } } },
        },
      });
    }
    return this.prismaService.pkg.findFirst({ where: { repoId, pkgNameId } });
  }

  getPkgs(userId?: string): Promise<Pkg[]> {
    if (userId) {
      return this.prismaService.pkg.findMany({
        where: {
          repo: { group: { members: { some: { userId } } } },
        },
        orderBy: { name: 'asc' },
      });
    }
    return this.prismaService.pkg.findMany({
      orderBy: { name: 'asc' },
    });
  }

  getPkgsByRepoId(repoId: string, userId?: string): Promise<Pkg[]> {
    if (userId) {
      return this.prismaService.pkg.findMany({
        where: {
          repoId,
          repo: { group: { members: { some: { userId } } } },
        },
      });
    }
    return this.prismaService.pkg.findMany({ where: { repoId } });
  }

  async createPkg(data: CreatePkgInputInternal, userId?: string): Promise<Pkg> {
    if (userId) {
      const repo = await this.prismaService.repo.findFirst({
        where: { id: data.repoId, group: { members: { some: { userId } } } },
      });
      if (!repo) {
        throw new UnprocessableEntityException('invalid repoId');
      }
    }
    return this.prismaService.pkg.create({ data });
  }

  async deletePkg(id: string, userId?: string): Promise<Pkg> {
    if (userId) {
      const pkg = await this.getPkg(id, userId);
      if (!pkg) {
        throw new NotFoundException();
      }
    }
    return this.prismaService.pkg.delete({ where: { id } });
  }

  deletePkgs(userId?: string): Promise<DeleteInfo> {
    if (userId) {
      return this.prismaService.pkg.deleteMany({
        where: {
          repo: { group: { members: { some: { userId } } } },
        },
      });
    }
    return this.prismaService.pkg.deleteMany();
  }

  deletePkgsByRepoId(repoId: string, userId?: string): Promise<DeleteInfo> {
    if (userId) {
      return this.prismaService.pkg.deleteMany({
        where: {
          repo: { id: repoId, group: { members: { some: { userId } } } },
        },
      });
    }
    return this.prismaService.pkg.deleteMany({ where: { id: repoId } });
  }
}
