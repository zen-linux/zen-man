import { CreateIsoInputInternal, Iso } from '@modules/iso/dtos/iso.dto';
import {
  ForbiddenException,
  Injectable,
  NotFoundException,
} from '@nestjs/common';
import { PrismaService } from './prisma.service';

@Injectable()
export class IsoDataService {
  constructor(private readonly prismaService: PrismaService) {}

  async createIso(data: CreateIsoInputInternal, userId?: string): Promise<Iso> {
    if (userId) {
      const user = await this.prismaService.user.findUnique({
        where: { id: userId },
      });
      if (!user) {
        throw new NotFoundException('invalid userId');
      }
      if (!user.isAdmin) {
        throw new ForbiddenException('only admins can create isos');
      }
    }
    return this.prismaService.iso.create({ data });
  }

  getIso(id: string): Promise<Iso | null> {
    return this.prismaService.iso.findFirst({ where: { id } });
  }

  getIsos(): Promise<Iso[]> {
    return this.prismaService.iso.findMany({
      orderBy: { createdAt: 'desc' },
    });
  }

  getIsosByArchId(archId: string): Promise<Iso[]> {
    return this.prismaService.iso.findMany({
      where: { archId },
      orderBy: { createdAt: 'desc' },
    });
  }

  async deleteIso(id: string, userId?: string): Promise<Iso> {
    if (userId) {
      const user = await this.prismaService.user.findUnique({
        where: { id: userId },
      });
      if (!user) {
        throw new NotFoundException('invalid userId');
      }
      if (!user.isAdmin) {
        throw new ForbiddenException('only admins can delete isos');
      }
    }
    const iso = await this.getIso(id);
    if (!iso) {
      throw new NotFoundException('iso not found');
    }
    return this.prismaService.iso.delete({ where: { id } });
  }
}
