import { Injectable, NotFoundException } from '@nestjs/common';
import {
  CreateGroupInput,
  Group,
  UpdateGroupInput,
} from '../../group/dtos/group.dto';
import { PrismaService } from './prisma.service';

@Injectable()
export class GroupDataService {
  constructor(private readonly prismaService: PrismaService) {}

  createGroup(data: CreateGroupInput): Promise<Group> {
    return this.prismaService.group.create({ data });
  }

  getGroup(id: string, userId?: string): Promise<Group | null> {
    if (userId) {
      return this.prismaService.group.findFirst({
        where: {
          id,
          members: { some: { userId } },
        },
      });
    }
    return this.prismaService.group.findUnique({ where: { id: id } });
  }

  getGroupByName(name: string, userId?: string): Promise<Group | null> {
    if (userId) {
      return this.prismaService.group.findFirst({
        where: { name, members: { some: { userId } } },
      });
    }
    return this.prismaService.group.findUnique({ where: { name } });
  }

  getGroups(userId?: string): Promise<Group[]> {
    if (userId) {
      return this.prismaService.group.findMany({
        where: { members: { some: { userId } } },
        orderBy: { createdAt: 'desc' },
      });
    }
    return this.prismaService.group.findMany({
      orderBy: { createdAt: 'desc' },
    });
  }

  async updateGroup(
    id: string,
    data: UpdateGroupInput,
    userId?: string,
  ): Promise<Group> {
    if (userId) {
      const group = await this.getGroup(id, userId);
      if (!group) {
        throw new NotFoundException();
      }
    }
    return this.prismaService.group.update({ where: { id }, data });
  }

  async deleteGroup(id: string, userId?: string): Promise<Group> {
    if (userId) {
      const group = await this.getGroup(id, userId);
      if (!group) {
        throw new NotFoundException();
      }
    }
    return this.prismaService.group.delete({ where: { id } });
  }
}
