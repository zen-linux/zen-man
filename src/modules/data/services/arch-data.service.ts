import {
  Arch,
  CreateArchInput,
  UpdateArchInput,
} from '@modules/arch/dtos/arch.dto';
import {
  ForbiddenException,
  Injectable,
  NotFoundException,
} from '@nestjs/common';
import { DeleteInfo } from '../dtos/delete-info.dto';
import { PrismaService } from './prisma.service';

@Injectable()
export class ArchDataService {
  constructor(private readonly prismaService: PrismaService) {}

  async createArch(data: CreateArchInput, userId?: string): Promise<Arch> {
    if (userId) {
      const user = await this.prismaService.user.findUnique({
        where: { id: userId },
      });
      if (!user) {
        throw new NotFoundException('invalid userId');
      }
      if (!user.isAdmin) {
        throw new ForbiddenException('only admins can create archs');
      }
    }
    return this.prismaService.arch.create({ data });
  }

  getArch(id: string): Promise<Arch | null> {
    return this.prismaService.arch.findFirst({ where: { id } });
  }

  getArchByName(name: string): Promise<Arch | null> {
    return this.prismaService.arch.findFirst({ where: { name } });
  }

  getArchs(): Promise<Arch[]> {
    return this.prismaService.arch.findMany({
      orderBy: { name: 'asc' },
    });
  }

  async updateArch(
    id: string,
    data: UpdateArchInput,
    userId?: string,
  ): Promise<Arch> {
    if (userId) {
      const user = await this.prismaService.user.findUnique({
        where: { id: userId },
      });
      if (!user) {
        throw new NotFoundException('invalid userId');
      }
      if (!user.isAdmin) {
        throw new ForbiddenException('only admins can update archs');
      }
    }
    return this.prismaService.arch.update({ where: { id }, data });
  }

  async deleteArch(id: string, userId?: string): Promise<Arch> {
    if (userId) {
      const user = await this.prismaService.user.findUnique({
        where: { id: userId },
      });
      if (!user) {
        throw new NotFoundException('invalid userId');
      }
      if (!user.isAdmin) {
        throw new ForbiddenException('only admins can delete archs');
      }
    }
    return this.prismaService.arch.delete({ where: { id } });
  }

  async deleteArchs(userId?: string): Promise<DeleteInfo> {
    if (userId) {
      const user = await this.prismaService.user.findUnique({
        where: { id: userId },
      });
      if (!user) {
        throw new NotFoundException('invalid userId');
      }
      if (!user.isAdmin) {
        throw new ForbiddenException('only admins can delete archs');
      }
    }
    return this.prismaService.arch.deleteMany();
  }
}
