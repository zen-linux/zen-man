import { PkgDependType } from '@modules/repo/constants';
import {
  CreatePkgDependInput,
  PkgDepend,
} from '@modules/repo/dtos/pkg-depend.dto';
import {
  Injectable,
  NotFoundException,
  UnprocessableEntityException,
} from '@nestjs/common';
import { DeleteInfo } from '../dtos/delete-info.dto';
import { PkgDataService } from './pkg-data.service';
import { PkgNameDataService } from './pkg-name-data.service';
import { PrismaService } from './prisma.service';

@Injectable()
export class PkgDependDataService {
  constructor(
    private readonly prismaService: PrismaService,
    private readonly pkgDataService: PkgDataService,
    private readonly pkgNameDataService: PkgNameDataService,
  ) {}

  getPkgDepend(id: string, userId?: string): Promise<PkgDepend | null> {
    if (userId) {
      return this.prismaService.pkgDepend.findFirst({
        where: {
          id,
          pkg: { repo: { group: { members: { some: { userId } } } } },
        },
      });
    }
    return this.prismaService.pkgDepend.findFirst({ where: { id } });
  }

  async getPkgDepends(userId?: string): Promise<PkgDepend[]> {
    if (userId) {
      return this.prismaService.pkgDepend.findMany({
        where: {
          pkg: { repo: { group: { members: { some: { userId } } } } },
        },
        orderBy: { pkg: { name: 'asc' } },
      });
    }
    return this.prismaService.pkgDepend.findMany({
      orderBy: { pkg: { name: 'asc' } },
    });
  }

  getPkgDependsByPkgIdAndType(
    pkgId: string,
    pkgDependType: PkgDependType,
    userId?: string,
  ): Promise<
    {
      name: string;
      version?: string;
      comparator?: string;
      comment?: string;
    }[]
  > {
    if (userId) {
      return this.prismaService.pkgDepend
        .findMany({
          where: {
            type: pkgDependType,
            pkg: {
              id: pkgId,
              repo: { group: { members: { some: { userId } } } },
            },
          },
          include: { pkgName: true },
          orderBy: { pkg: { name: 'asc' } },
        })
        .then((results) =>
          results.map((item) => ({
            name: item.pkgName.name,
            version: item.version || undefined,
            comparator: item.comparator || undefined,
            comment: item.comment || undefined,
          })),
        );
    }
    return this.prismaService.pkgDepend
      .findMany({
        where: { pkgId, type: pkgDependType },
        include: { pkgName: true },
        orderBy: { pkg: { name: 'asc' } },
      })
      .then((results) =>
        results.map((item) => ({
          name: item.pkgName.name,
          version: item.version || undefined,
          comparator: item.comparator || undefined,
          comment: item.comment || undefined,
        })),
      );
  }

  async createPkgDepend(
    data: CreatePkgDependInput,
    userId?: string,
  ): Promise<PkgDepend> {
    if (userId) {
      const pkg = await this.pkgDataService.getPkg(data.pkgId, userId);
      if (!pkg) {
        throw new UnprocessableEntityException(`invalid pkgId: ${data.pkgId}`);
      }
      const pkgName = await this.pkgNameDataService.getPkgName(data.pkgNameId);
      if (!pkgName) {
        throw new UnprocessableEntityException(
          `invalid pkgNameId: ${data.pkgNameId}`,
        );
      }
    }
    return this.prismaService.pkgDepend.create({ data });
  }

  async deletePkgDepend(id: string, userId?: string): Promise<PkgDepend> {
    if (userId) {
      const pkg = await this.getPkgDepend(id, userId);
      if (!pkg) {
        throw new NotFoundException();
      }
    }
    return this.prismaService.pkgDepend.delete({ where: { id } });
  }

  deletePkgDepends(userId?: string): Promise<DeleteInfo> {
    if (userId) {
      return this.prismaService.pkgDepend.deleteMany({
        where: {
          pkg: { repo: { group: { members: { some: { userId } } } } },
        },
      });
    }
    return this.prismaService.pkgDepend.deleteMany();
  }

  deletePkgDependsByPkgId(pkgId: string, userId?: string): Promise<DeleteInfo> {
    if (userId) {
      return this.prismaService.pkgDepend.deleteMany({
        where: {
          pkg: {
            id: pkgId,
            repo: { group: { members: { some: { userId } } } },
          },
        },
      });
    }
    return this.prismaService.pkgDepend.deleteMany({ where: { pkgId } });
  }

  deletePkgDependsByRepoId(
    repoId: string,
    userId?: string,
  ): Promise<DeleteInfo> {
    if (userId) {
      return this.prismaService.pkgDepend.deleteMany({
        where: {
          pkg: {
            repo: {
              id: repoId,
              group: {
                members: { some: { user: { id: userId, isAdmin: true } } },
              },
            },
          },
        },
      });
    }
    return this.prismaService.pkgDepend.deleteMany({
      where: { pkg: { repoId } },
    });
  }
}
