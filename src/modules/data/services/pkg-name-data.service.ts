import { PkgDependType } from '@modules/repo/constants';
import {
  CreatePkgNameInput,
  PkgName,
  UpdatePkgNameInput,
} from '@modules/repo/dtos/pkg-name.dto';
import { Injectable, NotFoundException } from '@nestjs/common';
import { DeleteInfo } from '../dtos/delete-info.dto';
import { PrismaService } from './prisma.service';

@Injectable()
export class PkgNameDataService {
  constructor(private readonly prismaService: PrismaService) {}

  async createPkgName(data: CreatePkgNameInput): Promise<PkgName> {
    return this.prismaService.pkgName.create({ data });
  }

  getPkgName(id: string): Promise<PkgName | null> {
    return this.prismaService.pkgName.findFirst({ where: { id } });
  }

  getPkgNameByName(name: string): Promise<PkgName | null> {
    return this.prismaService.pkgName.findFirst({ where: { name } });
  }

  // TODO check permissions
  getPkgNameByPkgIdAndType(
    pkgId: string,
    type: PkgDependType,
  ): Promise<PkgName[]> {
    return this.prismaService.pkgName.findMany({
      where: { depends: { some: { pkgId, type } } },
    });
  }

  async updatePkgName(id: string, data: UpdatePkgNameInput): Promise<PkgName> {
    const pkgName = await this.getPkgName(id);
    if (!pkgName) {
      throw new NotFoundException();
    }
    return this.prismaService.pkgName.update({ where: { id }, data });
  }

  async deletePkgName(id: string): Promise<PkgName> {
    const pkgName = await this.getPkgName(id);
    if (!pkgName) {
      throw new NotFoundException();
    }
    return this.prismaService.pkgName.delete({ where: { id } });
  }

  deletePkgNames(): Promise<DeleteInfo> {
    return this.prismaService.pkgName.deleteMany();
  }
}
