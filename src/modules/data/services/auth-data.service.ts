import { ApiKey } from '@modules/api-key/dtos/api-key.dto';
import { UserWithPassword } from '@modules/user/dtos/user.dto';
import { Injectable } from '@nestjs/common';
import { PrismaService } from './prisma.service';

@Injectable()
export class AuthDataService {
  constructor(private readonly prismaService: PrismaService) {}

  getApiKeyByKey(key: string): Promise<ApiKey | null> {
    return this.prismaService.apiKey
      .findUnique({ where: { key } })
      .then((apiKey) => {
        if (apiKey) {
          this.prismaService.apiKey
            .update({
              where: { id: apiKey.id },
              data: {
                lastUsed: new Date(),
              },
            })
            .then();
        }
        return apiKey;
      });
  }

  getUserByEmail(email: string): Promise<UserWithPassword | null> {
    return this.prismaService.user.findUnique({ where: { email } });
  }
}
