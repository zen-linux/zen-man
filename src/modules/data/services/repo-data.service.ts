import {
  CreateRepoInput,
  Repo,
  UpdateRepoInput,
} from '@modules/repo/dtos/repo.dto';
import {
  ForbiddenException,
  Injectable,
  NotFoundException,
  UnprocessableEntityException,
} from '@nestjs/common';
import { DeleteInfo } from '../dtos/delete-info.dto';
import { GroupDataService } from './group-data.service';
import { MemberDataService } from './member-data.service';
import { PrismaService } from './prisma.service';
import { UserDataService } from './user-data.service';

@Injectable()
export class RepoDataService {
  constructor(
    private readonly prismaService: PrismaService,
    private readonly groupDataService: GroupDataService,
    private readonly memberDataService: MemberDataService,
    private readonly userDataService: UserDataService,
  ) {}

  async createRepo(data: CreateRepoInput, userId?: string): Promise<Repo> {
    if (userId) {
      const group = await this.prismaService.group.findFirst({
        where: {
          id: data.groupId,
          members: { some: { userId } },
        },
      });
      if (!group) {
        throw new NotFoundException('invalid group id');
      }
    }
    return this.prismaService.repo.create({ data });
  }

  getRepo(id: string, userId?: string): Promise<Repo | null> {
    if (userId) {
      return this.prismaService.repo.findFirst({
        where: { id, group: { members: { some: { userId } } } },
      });
    }
    return this.prismaService.repo.findUnique({ where: { id } });
  }

  getRepoByName(repoName: string, userId?: string): Promise<Repo | null> {
    if (userId) {
      return this.prismaService.repo.findFirst({
        where: {
          name: repoName,
          group: { members: { some: { userId } } },
        },
      });
    }
    return this.prismaService.repo.findFirst({
      where: { name: repoName },
    });
  }

  getRepoByGroupNameAndRepoName(
    groupName: string,
    repoName: string,
    userId?: string,
  ): Promise<Repo | null> {
    if (userId) {
      return this.prismaService.repo.findFirst({
        where: {
          name: repoName,
          group: { name: groupName, members: { some: { userId } } },
        },
      });
    }
    return this.prismaService.repo.findFirst({
      where: { name: repoName, group: { name: groupName } },
    });
  }

  getReposByGroupId(groupId: string, userId: string): Promise<Repo[]> {
    return this.prismaService.repo.findMany({
      where: { groupId, group: { members: { some: { userId } } } },
    });
  }

  getRepos(userId?: string): Promise<Repo[]> {
    if (userId) {
      return this.prismaService.repo.findMany({
        where: { group: { members: { some: { userId } } } },
        orderBy: { name: 'asc' },
      });
    }
    return this.prismaService.repo.findMany({
      orderBy: { name: 'asc' },
    });
  }

  async updateRepo(
    id: string,
    data: UpdateRepoInput,
    userId?: string,
  ): Promise<Repo> {
    if (userId) {
      const repo = await this.getRepo(id, userId);
      if (!repo) {
        throw new NotFoundException();
      }
    }
    return this.prismaService.repo.update({ where: { id }, data });
  }

  async deleteRepo(id: string, userId?: string): Promise<Repo> {
    if (userId) {
      const user = await this.userDataService.getUser(userId);
      if (!user) {
        throw new NotFoundException('user not found');
      }
      const repo = await this.getRepo(id, userId);
      if (!repo) {
        throw new NotFoundException('repo not found');
      }
      const group = await this.groupDataService.getGroup(repo.groupId);
      if (!group) {
        throw new NotFoundException('group not found');
      }
      if (group.name === 'zen') {
        if (!user.isAdmin) {
          throw new ForbiddenException(
            'only admins can delete official zen repos',
          );
        }
        if (repo.name === 'core') {
          throw new UnprocessableEntityException(
            'cannot delete the official zen core repo',
          );
        }
      }
      const member = await this.memberDataService.getMemberForGroupIdAndUserId(
        group.id,
        userId,
      );
      if (!member) {
        throw new NotFoundException('member not found');
      }
      if (!member.isAdmin) {
        throw new ForbiddenException('only group admins can delete repos');
      }
    }
    return this.prismaService.repo.delete({ where: { id } });
  }

  deleteRepos(userId?: string): Promise<DeleteInfo> {
    if (userId) {
      return this.prismaService.repo.deleteMany({
        where: { group: { members: { some: { userId } } } },
      });
    }
    return this.prismaService.repo.deleteMany();
  }
}
