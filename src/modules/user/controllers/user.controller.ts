import { AuthUserId } from '@core/decorators/auth-user-id.decorator';
import { UserDataService } from '@modules/data/services/user-data.service';
import {
  Controller,
  Get,
  NotFoundException,
  Param,
  UnauthorizedException,
} from '@nestjs/common';
import { MyUser } from '../dtos/user.dto';

@Controller({
  path: 'users',
  version: '1',
})
export class UserController {
  constructor(private readonly userDataService: UserDataService) {}

  @Get('self')
  async self(@AuthUserId() userId: string): Promise<MyUser> {
    const user = await this.userDataService.getUser(userId);
    if (!user) {
      throw new UnauthorizedException();
    }
    return user;
  }

  @Get(':id')
  async getUser(
    @Param('id') id: string,
    @AuthUserId() userId: string,
  ): Promise<MyUser> {
    const user = await this.userDataService.getUser(id, userId);
    if (!user) {
      throw new NotFoundException();
    }
    return user;
  }

  @Get()
  getUsers(@AuthUserId() userId: string): Promise<MyUser[]> {
    return this.userDataService.getUsers(userId);
  }
}
