import { IsNotEmpty, IsString, MaxLength } from 'class-validator';

export const USER_NAME_MAX_LENGTH = 20;

export class CreateUserInput {
  @IsString()
  @IsNotEmpty()
  @MaxLength(USER_NAME_MAX_LENGTH)
  name!: string;

  @IsString()
  @IsNotEmpty()
  email!: string;

  @IsString()
  @IsNotEmpty()
  password!: string;
}

export class UpdateUserInput {
  @IsString()
  @IsNotEmpty()
  @MaxLength(USER_NAME_MAX_LENGTH)
  name?: string;
}

export class MyUser {
  id!: string;

  name!: string;

  email!: string;

  isAdmin!: boolean;

  createdAt!: Date;

  modifiedAt!: Date;
}

export class UserWithPassword extends MyUser {
  password!: string;
}
