import { Module } from '@nestjs/common';
import { UserController } from './controllers/user.controller';
import { UniqueEmailValidator } from './validators/unique-email.validator';

@Module({
  controllers: [UserController],
  providers: [UniqueEmailValidator],
})
export class UserModule {}
