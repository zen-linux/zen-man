import { Injectable, UnprocessableEntityException } from '@nestjs/common';

import {
  ValidatorConstraint,
  ValidatorConstraintInterface,
} from 'class-validator';
import { UserDataService } from '../../data/services/user-data.service';

@ValidatorConstraint({ name: 'unique-email', async: true })
@Injectable()
export class UniqueEmailValidator implements ValidatorConstraintInterface {
  constructor(private readonly userService: UserDataService) {}

  async validate(email: string): Promise<boolean> {
    return this.userService.getUserByEmail(email).then((user) => {
      if (user) {
        throw new UnprocessableEntityException('Email already exists');
      } else {
        return true;
      }
    });
  }
}
