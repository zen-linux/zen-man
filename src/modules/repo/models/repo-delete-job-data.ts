export interface RepoDeleteJobData {
  userId: string;
  repoId: string;
}
