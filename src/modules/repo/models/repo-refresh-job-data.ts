export interface RepoRefreshJobData {
  userId: string;
  repoId: string;
}
