import { Group } from '@modules/group/dtos/group.dto';
import { Member } from '@modules/member/dtos/member.dto';
import { MyUser } from '@modules/user/dtos/user.dto';
import { Repo } from '../dtos/repo.dto';

export interface PkgDeleteJobData {
  user: MyUser;
  member: Member;
  group: Group;
  repo: Repo;
  pkgName: string;
}
