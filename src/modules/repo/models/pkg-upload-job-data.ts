export interface PkgUploadJobData {
  userId: string;
  repoId: string;
  path: string;
  md5: string;
  sha256: string;
}
