export interface RepoCleanupJobData {
  userId: string;
  repoId: string;
}
