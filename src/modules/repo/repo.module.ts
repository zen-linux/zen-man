import { GzipModule } from '@modules/gzip/gzip.module';
import { ParserModule } from '@modules/parser/parser.module';
import { s3StorageEngine } from '@modules/s3/s3-storage-engine';
import { S3Module } from '@modules/s3/s3.module';
import { S3Service } from '@modules/s3/services/s3.service';
import { TemplateModule } from '@modules/template/template.module';
import { BullModule, InjectQueue } from '@nestjs/bull';
import { Module, OnModuleDestroy } from '@nestjs/common';
import { MulterModule } from '@nestjs/platform-express';
import { MulterOptions } from '@nestjs/platform-express/multer/interfaces/multer-options.interface';
import { Queue } from 'bull';
import {
  PKG_DELETE_QUEUE,
  PKG_UPLOAD_QUEUE,
  REPO_CLEANUP_QUEUE,
  REPO_DELETE_QUEUE,
  REPO_REFRESH_QUEUE,
} from './constants';
import { PkgController } from './controllers/pkg.controller';
import { RepoJobController } from './controllers/repo-job.controller';
import { RepoController } from './controllers/repo.controller';
import { PkgDeleteProcessor } from './processors/pkg-delete.processor';
import { PkgUploadProcessor } from './processors/pkg-upload.processor';
import { RepoCleanupProcessor } from './processors/repo-cleanup.processor';
import { RepoDeleteProcessor } from './processors/repo-delete.processor';
import { RepoRefreshProcessor } from './processors/repo-refresh.processor';
import { PkgDependService } from './services/pkg-depend.service';
import { RepoJobService } from './services/repo-job.service';
import { RepoService } from './services/repo.service';

@Module({
  imports: [
    BullModule.registerQueue(
      { name: PKG_UPLOAD_QUEUE },
      { name: PKG_DELETE_QUEUE },
      { name: REPO_DELETE_QUEUE },
      { name: REPO_REFRESH_QUEUE },
      { name: REPO_CLEANUP_QUEUE },
    ),
    GzipModule,
    MulterModule.registerAsync({
      imports: [S3Module],
      inject: [S3Service],
      useFactory: (s3: S3Service) => {
        const multerOptions: MulterOptions = {
          storage: s3StorageEngine({
            s3Client: s3.client,
            bucket: s3.reposBucket,
          }),
        };
        return multerOptions;
      },
    }),
    S3Module,
    TemplateModule,
    ParserModule,
    S3Module,
  ],
  controllers: [PkgController, RepoController, RepoJobController],
  providers: [
    RepoService,
    RepoJobService,
    PkgDependService,
    PkgUploadProcessor,
    PkgDeleteProcessor,
    RepoDeleteProcessor,
    RepoRefreshProcessor,
    RepoCleanupProcessor,
  ],
  exports: [PkgDependService, RepoService, RepoJobService],
})
export class RepoModule implements OnModuleDestroy {
  constructor(
    @InjectQueue(PKG_UPLOAD_QUEUE)
    private readonly pkgUploadQueue: Queue,
    @InjectQueue(PKG_DELETE_QUEUE)
    private readonly pkgDeleteQueue: Queue,
    @InjectQueue(REPO_REFRESH_QUEUE)
    private readonly repoRefreshQueue: Queue,
    @InjectQueue(REPO_CLEANUP_QUEUE)
    private readonly repoCleanupQueue: Queue,
    @InjectQueue(REPO_DELETE_QUEUE)
    private readonly repoDeleteQueue: Queue,
  ) {}

  async onModuleDestroy() {
    await this.pkgUploadQueue.close();
    await this.pkgDeleteQueue.close();
    await this.repoRefreshQueue.close();
    await this.repoCleanupQueue.close();
    await this.repoDeleteQueue.close();
  }
}
