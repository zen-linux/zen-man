import { RepoDataService } from '@modules/data/services/repo-data.service';
import { UserDataService } from '@modules/data/services/user-data.service';
import { RepoService } from '@modules/repo/services/repo.service';
import {
  OnQueueActive,
  OnQueueCompleted,
  OnQueueFailed,
  Process,
  Processor,
} from '@nestjs/bull';
import { Logger, NotFoundException } from '@nestjs/common';
import { Job } from 'bull';
import { REPO_REFRESH_QUEUE } from '../constants';
import { RepoRefreshJobData } from '../models/repo-refresh-job-data';

@Processor(REPO_REFRESH_QUEUE)
export class RepoRefreshProcessor {
  private readonly logger = new Logger(RepoRefreshProcessor.name);

  constructor(
    private readonly repoDataService: RepoDataService,
    private readonly userDataService: UserDataService,
    private readonly repoService: RepoService,
  ) {}

  @OnQueueActive()
  onActive(job: Job) {
    this.logger.log(`${job.id} started`);
  }

  @OnQueueCompleted()
  onCompleted(job: Job) {
    this.logger.log(`${job.id} completed`);
  }

  @OnQueueFailed()
  onFailed(job: Job) {
    this.logger.error(`${job.id} failed:`, job.stacktrace);
  }

  @Process()
  async refresh(job: Job<RepoRefreshJobData>): Promise<void> {
    const repo = await this.repoDataService.getRepo(job.data.repoId);
    if (!repo) {
      throw new NotFoundException('invalid repoId');
    }

    const user = await this.userDataService.getUser(job.data.userId);
    if (!user) {
      throw new NotFoundException('invalid userId');
    }

    //console.log('refresh repo:', repo.id, 'user:', user.id);

    await this.repoService.refreshRepo(repo.id, user.id, (progress) => {
      job.progress(progress);
      this.logger.log(
        `${job.id} progress | pkgsTotal: ${progress.pkgsTotal}, pkgsDone: ${
          progress.pkgsDone
        }, percentDone: ${progress.percentDone.toFixed(2)}`,
      );
    });
  }
}
