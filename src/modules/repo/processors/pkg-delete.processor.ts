import {
  OnQueueActive,
  OnQueueCompleted,
  OnQueueError,
  OnQueueFailed,
  Process,
  Processor,
} from '@nestjs/bull';
import { Logger } from '@nestjs/common';
import { Job } from 'bull';
import { PKG_DELETE_QUEUE } from '../constants';
import { PkgDeleteJobData } from '../models/pkg-delete-job-data';
import { RepoService } from '../services/repo.service';

@Processor(PKG_DELETE_QUEUE)
export class PkgDeleteProcessor {
  private readonly logger = new Logger(PkgDeleteProcessor.name);

  constructor(private readonly repoService: RepoService) {}

  @OnQueueActive()
  onActive(job: Job) {
    this.logger.log(
      `started job '${job.id}' '${job.data.group.name}/${job.data.repo.name}/${job.data.pkgName}'`,
    );
  }

  @OnQueueCompleted()
  onCompleted(job: Job) {
    this.logger.log(
      `completed job '${job.id}' '${job.data.group.name}/${job.data.repo.name}/${job.data.pkgName}'`,
    );
  }

  @OnQueueFailed()
  onFailed(job: Job, error: Error) {
    this.logger.error(
      `job '${job.id}' '${job.data.group.name}/${job.data.repo.name}/${job.data.pkgName}' failed: ${error.message}`,
    );
  }

  @OnQueueError()
  onError(error: unknown) {
    this.logger.error(`job error:`, error);
  }

  @Process()
  async deletePkg(job: Job<PkgDeleteJobData>): Promise<void> {
    const { deletedPkg, deletedInRepoFile } = await this.repoService.deletePkg(
      job.data.pkgName,
      job.data.repo,
      job.data.user,
    );

    if (deletedPkg && !deletedInRepoFile) {
      this.logger.warn(`package '${job.data.pkgName}' not found in repo file`);
    }

    if (!deletedPkg && deletedInRepoFile) {
      this.logger.warn(`package '${job.data.pkgName}' not found in database`);
    }

    if (!deletedPkg && !deletedInRepoFile) {
      throw new Error(`package '${job.data.pkgName}' not found`);
    }
  }
}
