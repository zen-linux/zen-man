import { RepoDataService } from '@modules/data/services/repo-data.service';
import { UserDataService } from '@modules/data/services/user-data.service';
import { RepoService } from '@modules/repo/services/repo.service';
import {
  OnQueueActive,
  OnQueueCompleted,
  OnQueueFailed,
  Process,
  Processor,
} from '@nestjs/bull';
import { Logger, NotFoundException } from '@nestjs/common';
import { Job } from 'bull';
import { REPO_CLEANUP_QUEUE } from '../constants';
import { RepoCleanupJobData } from '../models/repo-cleanup-job-data';

@Processor(REPO_CLEANUP_QUEUE)
export class RepoCleanupProcessor {
  private readonly logger = new Logger(RepoCleanupProcessor.name);

  constructor(
    private readonly repoDataService: RepoDataService,
    private readonly userDataService: UserDataService,
    private readonly repoService: RepoService,
  ) {}

  @OnQueueActive()
  onActive(job: Job) {
    this.logger.log(`${job.id} started`);
  }

  @OnQueueCompleted()
  onCompleted(job: Job) {
    this.logger.log(`${job.id} completed`);
  }

  @OnQueueFailed()
  onFailed(job: Job) {
    this.logger.error(`${job.id} failed:`, job.stacktrace);
  }

  @Process()
  async cleanup(job: Job<RepoCleanupJobData>): Promise<void> {
    const repo = await this.repoDataService.getRepo(job.data.repoId);
    if (!repo) {
      throw new NotFoundException('invalid repoId');
    }

    const user = await this.userDataService.getUser(job.data.userId);
    if (!user) {
      throw new NotFoundException('invalid userId');
    }

    //console.log('cleanup repo:', repo.id, 'user:', user.id);

    await this.repoService.cleanupRepo(repo.id, user.id, (progress) => {
      job.progress(progress);
      /*this.logger.log(
        `${job.id} progress | pkgsInDatabase: ${
          progress.pkgsInDatabase
        }, pkgsInFilesystem: ${progress.pkgsInFilesystem}, pkgsDeleted: ${
          progress.pkgsDeleted
        }, pkgsToDelete: ${
          progress.pkgsToDelete
        }, percentDone: ${progress.percentDone.toFixed(2)}`
      );*/
    });
  }
}
