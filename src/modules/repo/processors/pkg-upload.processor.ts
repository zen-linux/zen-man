import { GetObjectCommand } from '@aws-sdk/client-s3';
import { RepoDataService } from '@modules/data/services/repo-data.service';
import { UserDataService } from '@modules/data/services/user-data.service';
import { GzipService } from '@modules/gzip/services/gzip.service';
import { ParserService } from '@modules/parser/services/parser.service';
import { S3Service } from '@modules/s3/services/s3.service';
import {
  OnQueueActive,
  OnQueueCompleted,
  OnQueueError,
  OnQueueFailed,
  Process,
  Processor,
} from '@nestjs/bull';
import { Logger, NotFoundException } from '@nestjs/common';
import { Job } from 'bull';
import { Readable } from 'stream';
import { PKG_UPLOAD_QUEUE } from '../constants';
import { PkgUploadJobData } from '../models/pkg-upload-job-data';
import { RepoService } from '../services/repo.service';

@Processor(PKG_UPLOAD_QUEUE)
export class PkgUploadProcessor {
  private readonly logger = new Logger(PkgUploadProcessor.name);

  constructor(
    private readonly repoDataService: RepoDataService,
    private readonly userDataService: UserDataService,
    private readonly repoService: RepoService,
    private readonly parserService: ParserService,
    private readonly gzipService: GzipService,
    private readonly s3: S3Service,
  ) {}

  @OnQueueActive()
  onActive(job: Job) {
    this.logger.log(`job '${job.id}' started`);
  }

  @OnQueueCompleted()
  onCompleted(job: Job) {
    this.logger.log(`job '${job.id}' completed`);
  }

  @OnQueueFailed()
  onFailed(job: Job, error: Error) {
    this.logger.error(`job '${job.id}' failed: ${error.message}`);
  }

  @OnQueueError()
  onError(error: unknown) {
    this.logger.error(`job error:`, error);
  }

  @Process()
  async uploadPkg(job: Job<PkgUploadJobData>): Promise<void> {
    //console.log('job data:', job.data);

    const repo = await this.repoDataService.getRepo(
      job.data.repoId,
      job.data.userId,
    );
    if (!repo) {
      throw new NotFoundException('invalid repoId');
    }
    //console.log('repo:', repo);

    const user = await this.userDataService.getUser(job.data.userId);
    if (!user) {
      throw new NotFoundException('invalid userId');
    }
    //console.log('user:', user);

    const stats = await this.s3.stat(job.data.path, {
      bucket: this.s3.reposBucket,
    });
    if (!stats) {
      throw new NotFoundException('uploaded file not found');
    }
    //console.log('stats:', stats);

    //console.log('md5:', md5);
    //console.log('sha256:', sha256);

    console.log('start getObject request');
    const getObjectResponse = await this.s3.client.send(
      new GetObjectCommand({
        Bucket: this.s3.reposBucket,
        Key: job.data.path,
      }),
    );
    console.log('got getObject response');
    if (!(getObjectResponse.Body instanceof Readable)) {
      throw new Error('body is not readable');
    }

    const pkgInfo = this.parserService.parsePkgInfo(
      await this.gzipService.readFileContentFromGzipStream(
        getObjectResponse.Body,
        '.PKGINFO',
      ),
    );
    pkgInfo.sizeC = stats.size;
    pkgInfo.md5 = job.data.md5;
    pkgInfo.sha256 = job.data.sha256;

    console.log('pkgInfo:', pkgInfo);

    const { repoPath, dbPath } = await this.repoService.getRepoPaths(repo);
    console.log('got repo path:', repoPath);

    const fileName = this.repoService.getPkgFileName(
      pkgInfo.name,
      pkgInfo.ver,
      pkgInfo.rel,
      pkgInfo.arch,
    );

    const pkg = await this.repoService.createPkgFromPkgInfo(
      repo.id,
      user.id,
      pkgInfo,
    );
    console.log('created pkg:', pkg);

    await this.s3.mv(job.data.path, `${repoPath}/${fileName}`, {
      bucket: this.s3.reposBucket,
      public: true,
      metaData: {
        md5: job.data.md5,
        sha256: job.data.sha256,
        uploader: user.id,
      },
    });
    console.log('uploaded file to s3:', `${repoPath}/${fileName}`);

    await this.repoService.addPkgToDbFile(dbPath, pkg);
    console.log('updated db file');

    await this.repoService.updateIndexHtml(repoPath);

    //await unlink(job.data.path);
    //console.log('deleted file:', job.data.filePath);
  }
}
