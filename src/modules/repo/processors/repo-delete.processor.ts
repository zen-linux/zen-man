import { RepoDataService } from '@modules/data/services/repo-data.service';
import { UserDataService } from '@modules/data/services/user-data.service';
import { GzipService } from '@modules/gzip/services/gzip.service';
import { ParserService } from '@modules/parser/services/parser.service';
import { RepoService } from '@modules/repo/services/repo.service';
import { S3Service } from '@modules/s3/services/s3.service';
import {
  OnQueueActive,
  OnQueueCompleted,
  OnQueueError,
  OnQueueFailed,
  OnQueueProgress,
  Process,
  Processor,
} from '@nestjs/bull';
import { Logger } from '@nestjs/common';
import { Job } from 'bull';
import { REPO_DELETE_QUEUE } from '../constants';
import { RepoDeleteJobData } from '../models/repo-delete-job-data';

@Processor(REPO_DELETE_QUEUE)
export class RepoDeleteProcessor {
  private readonly logger = new Logger(RepoDeleteProcessor.name);

  constructor(
    private readonly repoDataService: RepoDataService,
    private readonly userDataService: UserDataService,
    private readonly repoService: RepoService,
    private readonly s3: S3Service,
    private readonly parserService: ParserService,
    private readonly gzipService: GzipService,
  ) {}

  @OnQueueActive()
  onActive(job: Job) {
    this.logger.log(`job '${job.id}' started`);
  }

  @OnQueueProgress()
  onProgress(job: Job, progress: unknown) {
    this.logger.log(`job '${job.id}' progress: ${progress}`);
  }

  @OnQueueCompleted()
  onCompleted(job: Job) {
    this.logger.log(`job '${job.id}' completed`);
  }

  @OnQueueFailed()
  onFailed(job: Job, error: Error) {
    this.logger.error(`job '${job.id}' failed: ${error.message}`);
  }

  @OnQueueError()
  onError(error: Error) {
    this.logger.error(`job error:`, error);
  }

  @Process()
  async deleteRepo(job: Job<RepoDeleteJobData>): Promise<void> {
    console.log('deleteRepoJob:', job.data);
  }
}
