import { AuthUserId } from '@core/decorators/auth-user-id.decorator';
import { Job } from '@core/models/job';
import { RepoDataService } from '@modules/data/services/repo-data.service';
import {
  Body,
  Controller,
  Delete,
  Get,
  NotFoundException,
  Param,
  Post,
  Put,
} from '@nestjs/common';
import { CreateRepoInput, Repo, UpdateRepoInput } from '../dtos/repo.dto';
import { RepoService } from '../services/repo.service';

@Controller({
  path: 'repos',
  version: '1',
})
export class RepoController {
  constructor(
    private readonly repoDataService: RepoDataService,
    private readonly repoService: RepoService,
  ) {}

  @Get()
  getRepos(@AuthUserId() userId: string): Promise<Repo[]> {
    return this.repoDataService.getRepos(userId);
  }

  @Get('name/:groupName/:repoName')
  async getRepoByName(
    @Param('groupName') groupName: string,
    @Param('repoName') repoName: string,
    @AuthUserId() userId: string,
  ): Promise<Repo> {
    const repo = await this.repoDataService.getRepoByGroupNameAndRepoName(
      groupName,
      repoName,
      userId,
    );
    if (!repo) {
      throw new NotFoundException();
    }
    return repo;
  }

  @Get(':id')
  async getRepo(
    @Param('id') id: string,
    @AuthUserId() userId: string,
  ): Promise<Repo> {
    const repo = await this.repoDataService.getRepo(id, userId);
    if (!repo) {
      throw new NotFoundException();
    }
    return repo;
  }

  @Post()
  createRepo(
    @Body() data: CreateRepoInput,
    @AuthUserId() userId: string,
  ): Promise<Repo> {
    return this.repoDataService.createRepo(data, userId);
  }

  @Put(':id')
  updateRepo(
    @Param('id') id: string,
    @Body() data: UpdateRepoInput,
    @AuthUserId() userId: string,
  ): Promise<Repo> {
    return this.repoDataService.updateRepo(id, data, userId);
  }

  @Delete(':id')
  async deleteRepo(
    @Param('id') id: string,
    @AuthUserId() userId: string,
  ): Promise<Job> {
    return this.repoService.deleteRepo(id, userId);
  }
}
