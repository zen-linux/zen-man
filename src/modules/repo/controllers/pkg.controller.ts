import { AuthUserId } from '@core/decorators/auth-user-id.decorator';
import { Job } from '@core/models/job';
import { MulterFile } from '@core/models/multer-file';
import { PkgDataService } from '@modules/data/services/pkg-data.service';
import { RepoJobService } from '@modules/repo/services/repo-job.service';
import {
  Body,
  Controller,
  Delete,
  Get,
  NotFoundException,
  Param,
  Post,
  UploadedFile,
  UseInterceptors,
} from '@nestjs/common';
import { FileInterceptor } from '@nestjs/platform-express';
import { CreatePkgInput, Pkg } from '../dtos/pkg.dto';
import { RepoService } from '../services/repo.service';

@Controller({
  version: '1',
})
export class PkgController {
  constructor(
    private readonly pkgDataService: PkgDataService,
    private readonly repoJobService: RepoJobService,
    private readonly repoService: RepoService,
  ) {}

  @Get('pkgs')
  getPkgs(@AuthUserId() userId: string): Promise<Pkg[]> {
    return this.pkgDataService.getPkgs(userId);
  }

  @Get('pkgs/:id')
  async getPkg(
    @Param('id') id: string,
    @AuthUserId() userId: string,
  ): Promise<Pkg> {
    const pkg = await this.pkgDataService.getPkg(id, userId);
    if (!pkg) {
      throw new NotFoundException();
    }
    return pkg;
  }

  @Post('pkgs')
  @UseInterceptors(FileInterceptor('pkg'))
  uploadPkg(
    @Body() data: CreatePkgInput,
    @UploadedFile() pkg: MulterFile,
    @AuthUserId() userId: string,
  ): Promise<Job> {
    return this.repoJobService.addPkgUploadJob({
      repoId: data.repoId,
      userId,
      path: pkg.path,
      md5: pkg.md5,
      sha256: pkg.sha256,
    });
  }

  @Delete('repos/:groupName/:repoName/:pkgName')
  deletePkg(
    @Param('groupName') groupName: string,
    @Param('repoName') repoName: string,
    @Param('pkgName') pkgName: string,
    @AuthUserId() userId: string,
  ): Promise<Job> {
    return this.repoJobService.addPkgDeleteJob({
      groupName,
      repoName,
      pkgName,
      userId,
    });
  }
}
