import { AuthUserId } from '@core/decorators/auth-user-id.decorator';
import { Job } from '@core/models/job';
import {
  Body,
  Controller,
  Get,
  NotFoundException,
  Param,
  Post,
} from '@nestjs/common';
import { PkgDeleteJob } from '../dtos/pkg-delete-job.dto';
import { PkgUploadJob } from '../dtos/pkg-upload-job.dto';
import {
  CreateRepoCleanupJobInput,
  RepoCleanupJob,
} from '../dtos/repo-cleanup-job.dto';
import {
  CreateRepoRefreshJobInput,
  RepoRefreshJob,
} from '../dtos/repo-refresh-job.dto';
import { RepoJobService } from '../services/repo-job.service';

@Controller({
  version: '1',
})
export class RepoJobController {
  constructor(private readonly repoJobService: RepoJobService) {}

  @Get('pkg-upload-jobs')
  getPkgUploadJobs(@AuthUserId() userId: string): Promise<PkgUploadJob[]> {
    return this.repoJobService.getPkgUploadJobs(userId);
  }

  @Get('pkg-upload-jobs/:jobId')
  async getPkgUploadJob(
    @Param('jobId') jobId: string,
    @AuthUserId() userId: string,
  ): Promise<PkgUploadJob> {
    const job = await this.repoJobService.getPkgUploadJob(jobId, userId);
    if (!job) {
      throw new NotFoundException();
    }
    return job;
  }

  @Get('pkg-delete-jobs')
  getPkgDeleteJobs(@AuthUserId() userId: string): Promise<PkgDeleteJob[]> {
    return this.repoJobService.getPkgDeleteJobs(userId);
  }

  @Get('pkg-delete-jobs/:jobId')
  async getPkgDeleteJob(
    @Param('jobId') jobId: string,
    @AuthUserId() userId: string,
  ): Promise<PkgDeleteJob> {
    const job = await this.repoJobService.getPkgDeleteJob(jobId, userId);
    if (!job) {
      throw new NotFoundException();
    }
    return job;
  }

  @Get('repo-refresh-jobs')
  getRepoRefreshJobs(@AuthUserId() userId: string): Promise<RepoRefreshJob[]> {
    return this.repoJobService.getRepoRefreshJobs(userId);
  }

  @Post('repo-refresh-jobs')
  async createRepoRefreshJob(
    @Body() data: CreateRepoRefreshJobInput,
    @AuthUserId() userId: string,
  ): Promise<Job> {
    return this.repoJobService.addRepoRefreshJob({
      repoId: data.repoId,
      userId,
    });
  }

  @Get('repo-refresh-jobs/:jobId')
  async getRepoRefreshJob(
    @Param('jobId') jobId: string,
    @AuthUserId() userId: string,
  ): Promise<RepoRefreshJob> {
    const job = await this.repoJobService.getRepoRefreshJob(jobId, userId);
    if (!job) {
      throw new NotFoundException();
    }
    return job;
  }

  @Get('repo-cleanup-jobs')
  getRepoCleanupJobs(@AuthUserId() userId: string): Promise<RepoCleanupJob[]> {
    return this.repoJobService.getRepoCleanupJobs(userId);
  }

  @Post('repo-cleanup-jobs')
  async createRepoCleanupJob(
    @Body() data: CreateRepoCleanupJobInput,
    @AuthUserId() userId: string,
  ): Promise<Job> {
    return this.repoJobService.addRepoCleanupJob({
      repoId: data.repoId,
      userId,
    });
  }

  @Get('repo-cleanup-jobs/:jobId')
  async getRepoCleanupJob(
    @Param('jobId') jobId: string,
    @AuthUserId() userId: string,
  ): Promise<RepoCleanupJob> {
    const job = await this.repoJobService.getRepoCleanupJob(jobId, userId);
    if (!job) {
      throw new NotFoundException();
    }
    return job;
  }
}
