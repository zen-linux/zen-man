import { RepoDbService } from './repo-db.service';

fdescribe('RepoDbService', () => {
  describe('addPkg', () => {
    it('should add a pkg correctly', () => {
      //const service = new RepoDbService('');
      expect(1).toBe(1);
    });
  });

  describe('parseVersion', () => {
    it.each([
      ['0.0.1-0', { epoch: 0, version: '0.0.1', release: 0 }],
      ['0.0.1-1', { epoch: 0, version: '0.0.1', release: 1 }],
      ['0.0.1-10', { epoch: 0, version: '0.0.1', release: 10 }],
      ['0.0.1-101', { epoch: 0, version: '0.0.1', release: 101 }],
      ['1:0.0.1-1', { epoch: 1, version: '0.0.1', release: 1 }],
      ['2:0.0.1-1', { epoch: 2, version: '0.0.1', release: 1 }],
      ['10:0.0.1-1', { epoch: 10, version: '0.0.1', release: 1 }],
      ['101:0.0.1-1', { epoch: 101, version: '0.0.1', release: 1 }],
    ])('should parse correctly %p', (input, expected) => {
      const service = new RepoDbService('');
      expect(service.parseFullVersion(input)).toStrictEqual(expected);
    });
  });
});
