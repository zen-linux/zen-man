interface ParsedVersion {
  epoch: number;
  version: string;
  release: number;
}

export class RepoDbService {
  constructor(private readonly dbPath: string) {
    //
  }

  addPkg() {
    //
  }

  removePkg() {
    //
  }

  private prepareDb() {
    //
  }

  private createDb() {
    //
  }

  private rotateDb() {
    //
  }

  private cleanUp() {
    //
  }

  private findPkgEntry() {
    //
  }

  private formatEntry() {
    //
  }

  private writeDbEntry() {
    //
  }

  private removeDbEntry() {
    //
  }

  compareFullVersions(fullVersionA: string, fullVersionB: string): number {
    if (fullVersionA === fullVersionB) {
      return 0;
    }

    const parsedA = this.parseFullVersion(fullVersionA);
    const parsedB = this.parseFullVersion(fullVersionB);

    if (parsedA.epoch < parsedB.epoch) {
      return -1;
    } else if (parsedA.epoch > parsedB.epoch) {
      return 1;
    }

    /*const versionResult = this.compareVersions(
      parsedA.version,
      parsedB.version
    );
    if (versionResult !== 0) {
      return versionResult;
    }*/

    if (parsedA.release < parsedB.release) {
      return -1;
    } else if (parsedA.release > parsedB.release) {
      return 1;
    }

    return 0;
  }

  /*compareVersions(_versionA: string, _versionB: string): number {
    return 0;
  }*/

  parseFullVersion(version: string): ParsedVersion {
    const regex = /((?<epoch>\d*):)?(?<version>.*)-(?<release>\d*)/;

    const matches = regex.exec(version);
    if (matches === null) {
      throw new Error(`could not parse version string '${version}'`);
    }

    const groups = matches.groups;
    if (groups === undefined) {
      throw new Error('could not get parsed groups');
    }

    return {
      epoch: groups.epoch === undefined ? 0 : Number.parseInt(groups.epoch),
      version: groups.version,
      release: Number.parseInt(groups.release),
    };
  }
}
