import { Job } from '@core/models/job';
import { GroupDataService } from '@modules/data/services/group-data.service';
import { MemberDataService } from '@modules/data/services/member-data.service';
import { PkgDataService } from '@modules/data/services/pkg-data.service';
import { RepoDataService } from '@modules/data/services/repo-data.service';
import { UserDataService } from '@modules/data/services/user-data.service';
import { InjectQueue } from '@nestjs/bull';
import {
  ForbiddenException,
  Injectable,
  NotFoundException,
  UnprocessableEntityException,
} from '@nestjs/common';
import Bull, { Queue } from 'bull';
import { v4 as uuid } from 'uuid';
import {
  PKG_DELETE_QUEUE,
  PKG_UPLOAD_QUEUE,
  REPO_CLEANUP_QUEUE,
  REPO_DELETE_QUEUE,
  REPO_REFRESH_QUEUE,
} from '../constants';
import { PkgDeleteJob } from '../dtos/pkg-delete-job.dto';
import { PkgUploadJob } from '../dtos/pkg-upload-job.dto';
import { RepoCleanupJob } from '../dtos/repo-cleanup-job.dto';
import { RepoRefreshJob } from '../dtos/repo-refresh-job.dto';
import { PkgDeleteJobData } from '../models/pkg-delete-job-data';
import { PkgUploadJobData } from '../models/pkg-upload-job-data';
import { RepoCleanupJobData } from '../models/repo-cleanup-job-data';
import { RepoDeleteJobData } from '../models/repo-delete-job-data';
import { RepoRefreshJobData } from '../models/repo-refresh-job-data';

const enum JobPriority {
  HIGH = 30,
  MEDIUM = 60,
  LOW = 90,
}

@Injectable()
export class RepoJobService {
  constructor(
    private readonly groupDataService: GroupDataService,
    private readonly repoDataService: RepoDataService,
    private readonly pkgDataService: PkgDataService,
    private readonly userDataService: UserDataService,
    private readonly memberDataService: MemberDataService,
    @InjectQueue(REPO_REFRESH_QUEUE)
    private readonly repoRefreshQueue: Queue<RepoRefreshJobData>,
    @InjectQueue(REPO_CLEANUP_QUEUE)
    private readonly repoCleanupQueue: Queue<RepoCleanupJobData>,
    @InjectQueue(PKG_UPLOAD_QUEUE)
    private readonly pkgUploadQueue: Queue<PkgUploadJobData>,
    @InjectQueue(PKG_DELETE_QUEUE)
    private readonly pkgDeleteQueue: Queue<PkgDeleteJobData>,
    @InjectQueue(REPO_DELETE_QUEUE)
    private readonly repoDeleteQueue: Queue<RepoDeleteJobData>,
  ) {}
  async addRepoDeleteJob(data: RepoDeleteJobData): Promise<Job> {
    const bullJob = await this.repoDeleteQueue.add(data, {
      jobId: uuid(),
    });
    return {
      jobId:
        typeof bullJob.id === 'number' ? bullJob.id.toString() : bullJob.id,
    };
  }

  async addPkgUploadJob(data: PkgUploadJobData): Promise<Job> {
    const user = await this.userDataService.getUser(data.userId);
    if (!user) {
      throw new NotFoundException('user not found');
    }
    const repo = await this.repoDataService.getRepo(data.repoId, user.id);
    if (!repo) {
      throw new NotFoundException('repo not found');
    }
    const member = await this.memberDataService.getMemberForGroupIdAndUserId(
      repo.groupId,
      user.id,
    );
    if (!member) {
      throw new NotFoundException('member not found');
    }

    const bullJob = await this.pkgUploadQueue.add(data, {
      jobId: uuid(),
      priority: this.getJobPriority(user.isAdmin, member.isAdmin),
    });
    return {
      jobId:
        typeof bullJob.id === 'number' ? bullJob.id.toString() : bullJob.id,
    };
  }

  async addPkgDeleteJob(data: {
    groupName: string;
    repoName: string;
    pkgName: string;
    userId: string;
  }): Promise<Job> {
    const user = await this.userDataService.getUser(data.userId);
    if (!user) {
      throw new NotFoundException('user not found');
    }

    const repo = await this.repoDataService.getRepoByGroupNameAndRepoName(
      data.groupName,
      data.repoName,
      user.id,
    );
    if (!repo) {
      throw new NotFoundException('repo not found');
    }

    const group = await this.groupDataService.getGroup(repo.groupId, user.id);
    if (!group) {
      throw new NotFoundException('group not found');
    }

    const member = await this.memberDataService.getMemberForGroupIdAndUserId(
      repo.groupId,
      user.id,
    );
    if (!member) {
      throw new NotFoundException('member not found');
    }

    const bullJob = await this.pkgDeleteQueue.add(
      {
        user,
        member,
        group,
        repo,
        pkgName: data.pkgName,
      },
      {
        jobId: uuid(),
        priority: this.getJobPriority(user.isAdmin, member.isAdmin),
      },
    );
    return {
      jobId:
        typeof bullJob.id === 'number' ? bullJob.id.toString() : bullJob.id,
    };
  }

  async addRepoRefreshJob(data: RepoRefreshJobData): Promise<Job> {
    const repo = await this.repoDataService.getRepo(data.repoId, data.userId);
    if (!repo) {
      throw new UnprocessableEntityException('invalid repoId');
    }
    const user = await this.userDataService.getUser(data.userId);
    if (!user) {
      throw new UnprocessableEntityException('invalid userId');
    }
    if (!user.isAdmin) {
      throw new ForbiddenException('only admins can refresh repos');
    }

    const bullJob = await this.repoRefreshQueue.add(data, {
      jobId: uuid(),
      attempts: 1,
    });
    return {
      jobId:
        typeof bullJob.id === 'number' ? bullJob.id.toString() : bullJob.id,
    };
  }

  async addRepoCleanupJob(data: RepoCleanupJobData): Promise<Job> {
    const repo = await this.repoDataService.getRepo(data.repoId, data.userId);
    if (!repo) {
      throw new UnprocessableEntityException('invalid repoId');
    }
    const user = await this.userDataService.getUser(data.userId);
    if (!user) {
      throw new UnprocessableEntityException('invalid userId');
    }
    if (!user.isAdmin) {
      throw new ForbiddenException('only admins can refresh repos');
    }

    const bullJob = await this.repoCleanupQueue.add(data, {
      jobId: uuid(),
      attempts: 1,
    });
    return {
      jobId:
        typeof bullJob.id === 'number' ? bullJob.id.toString() : bullJob.id,
    };
  }

  async getRepoRefreshJob(
    id: string,
    userId?: string,
  ): Promise<RepoRefreshJob | null> {
    const bullJob = await this.repoRefreshQueue.getJob(id);
    if (!bullJob) {
      return null;
    }
    if (userId && bullJob.data.userId !== userId) {
      return null;
    }
    return this.mapBullJobToRepoRefreshJob(bullJob);
  }

  async getRepoCleanupJob(
    id: string,
    userId?: string,
  ): Promise<RepoCleanupJob | null> {
    const bullJob = await this.repoCleanupQueue.getJob(id);
    if (!bullJob) {
      return null;
    }
    if (userId && bullJob.data.userId !== userId) {
      return null;
    }
    return this.mapBullJobToRepoCleanupJob(bullJob);
  }

  async getPkgUploadJob(
    id: string,
    userId?: string,
  ): Promise<PkgUploadJob | null> {
    const bullJob = await this.pkgUploadQueue.getJob(id);
    if (!bullJob) {
      return null;
    }
    if (userId && bullJob.data.userId !== userId) {
      return null;
    }
    return this.mapBullJobToPkgUploadJob(bullJob);
  }

  async getPkgDeleteJob(
    id: string,
    userId?: string,
  ): Promise<PkgDeleteJob | null> {
    const bullJob = await this.pkgDeleteQueue.getJob(id);
    if (!bullJob) {
      return null;
    }
    if (userId && bullJob.data.user?.id !== userId) {
      return null;
    }
    return this.mapBullJobToPkgDeleteJob(bullJob);
  }

  async getRepoRefreshJobs(userId?: string): Promise<RepoRefreshJob[]> {
    let bullJobs = await this.repoRefreshQueue.getJobs([
      'active',
      'delayed',
      'paused',
      'waiting',
      'completed',
      'failed',
    ]);

    if (userId) {
      bullJobs = bullJobs.filter((bullJob) => bullJob.data.userId === userId);
    }

    const jobs = await Promise.all(
      bullJobs.map((bullJob) => this.mapBullJobToRepoRefreshJob(bullJob)),
    );

    return jobs.sort((a, b) => b.createdAt.getTime() - a.createdAt.getTime());
  }

  async getRepoCleanupJobs(userId?: string): Promise<RepoCleanupJob[]> {
    let bullJobs = await this.repoCleanupQueue.getJobs([
      'active',
      'delayed',
      'paused',
      'waiting',
      'completed',
      'failed',
    ]);

    if (userId) {
      bullJobs = bullJobs.filter((bullJob) => bullJob.data.userId === userId);
    }

    const jobs = await Promise.all(
      bullJobs.map((bullJob) => this.mapBullJobToRepoCleanupJob(bullJob)),
    );

    return jobs.sort((a, b) => b.createdAt.getTime() - a.createdAt.getTime());
  }

  async getPkgUploadJobs(userId?: string): Promise<PkgUploadJob[]> {
    let bullJobs = await this.pkgUploadQueue.getJobs([
      'active',
      'delayed',
      'paused',
      'waiting',
      'completed',
      'failed',
    ]);

    if (userId) {
      bullJobs = bullJobs.filter((bullJob) => bullJob.data.userId === userId);
    }

    const jobs = await Promise.all(
      bullJobs.map((bullJob) => this.mapBullJobToPkgUploadJob(bullJob)),
    );

    return jobs.sort((a, b) => b.createdAt.getTime() - a.createdAt.getTime());
  }

  async getPkgDeleteJobs(userId?: string): Promise<PkgDeleteJob[]> {
    let bullJobs = await this.pkgDeleteQueue.getJobs([
      'active',
      'delayed',
      'paused',
      'waiting',
      'completed',
      'failed',
    ]);

    if (userId) {
      bullJobs = bullJobs.filter((bullJob) => bullJob.data.user?.id === userId);
    }

    const jobs = await Promise.all(
      bullJobs.map((bullJob) => this.mapBullJobToPkgDeleteJob(bullJob)),
    );

    return jobs.sort((a, b) => b.createdAt.getTime() - a.createdAt.getTime());
  }

  private async mapBullJobToRepoRefreshJob(
    bullJob: Bull.Job<RepoRefreshJobData>,
  ): Promise<RepoRefreshJob> {
    return {
      id: typeof bullJob.id === 'number' ? bullJob.id.toString() : bullJob.id,
      state: await bullJob.getState(),
      progress: bullJob.progress(),
      createdAt: new Date(bullJob.timestamp),
    };
  }

  private async mapBullJobToRepoCleanupJob(
    bullJob: Bull.Job<RepoCleanupJobData>,
  ): Promise<RepoCleanupJob> {
    return {
      id: typeof bullJob.id === 'number' ? bullJob.id.toString() : bullJob.id,
      state: await bullJob.getState(),
      progress: bullJob.progress(),
      createdAt: new Date(bullJob.timestamp),
    };
  }

  private async mapBullJobToPkgUploadJob(
    bullJob: Bull.Job<PkgUploadJobData>,
  ): Promise<PkgUploadJob> {
    const job: PkgUploadJob = {
      id: typeof bullJob.id === 'number' ? bullJob.id.toString() : bullJob.id,
      state: await bullJob.getState(),
      createdAt: new Date(bullJob.timestamp),
    };

    if (bullJob.failedReason) {
      job.failedReason = bullJob.failedReason;
    }

    return job;
  }

  private async mapBullJobToPkgDeleteJob(
    bullJob: Bull.Job<PkgDeleteJobData>,
  ): Promise<PkgDeleteJob> {
    const job: PkgDeleteJob = {
      id: typeof bullJob.id === 'number' ? bullJob.id.toString() : bullJob.id,
      state: await bullJob.getState(),
      createdAt: new Date(bullJob.timestamp),
    };

    if (bullJob.failedReason) {
      job.failedReason = bullJob.failedReason;
    }

    return job;
  }

  private getJobPriority(isAdmin = false, isGroupAdmin = false): JobPriority {
    return isAdmin
      ? JobPriority.HIGH
      : isGroupAdmin
      ? JobPriority.MEDIUM
      : JobPriority.LOW;
  }
}
