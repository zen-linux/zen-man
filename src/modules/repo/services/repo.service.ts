import { TemplateService } from '@core/abstracts/template.service';
import { Job } from '@core/models/job';
import { ArchDataService } from '@modules/data/services/arch-data.service';
import { GroupDataService } from '@modules/data/services/group-data.service';
import { PkgDataService } from '@modules/data/services/pkg-data.service';
import { PkgDependDataService } from '@modules/data/services/pkg-depend-data.service';
import { PkgNameDataService } from '@modules/data/services/pkg-name-data.service';
import { PrismaService } from '@modules/data/services/prisma.service';
import { RepoDataService } from '@modules/data/services/repo-data.service';
import { UserDataService } from '@modules/data/services/user-data.service';
import { GzipService } from '@modules/gzip/services/gzip.service';
import { PkgInfo } from '@modules/parser/models/pkg-info';
import { ParserService } from '@modules/parser/services/parser.service';
import { PkgDependType } from '@modules/repo/constants';
import { S3Service } from '@modules/s3/services/s3.service';
import { MyUser } from '@modules/user/dtos/user.dto';
import { Injectable, Logger, NotFoundException } from '@nestjs/common';
import { createHash } from 'crypto';
import * as fs from 'fs';
import * as fsp from 'fs/promises';
import { minify } from 'html-minifier';
import * as path from 'path';
import { Readable } from 'stream';
import { extract, pack } from 'tar-stream';
import { file as tempFile } from 'tmp-promise';
import { createGunzip, createGzip } from 'zlib';
import { Pkg } from '../dtos/pkg.dto';
import { Repo } from '../dtos/repo.dto';
import { RepoJobService } from './repo-job.service';

@Injectable()
export class RepoService {
  private readonly logger = new Logger(RepoService.name);

  constructor(
    private readonly gzipFileService: GzipService,
    private readonly s3: S3Service,
    private readonly templateService: TemplateService,
    private readonly parserService: ParserService,
    private readonly repoDataService: RepoDataService,
    private readonly pkgDataService: PkgDataService,
    private readonly pkgNameDataService: PkgNameDataService,
    private readonly archDataService: ArchDataService,
    private readonly groupDataService: GroupDataService,
    private readonly pkgDependDataService: PkgDependDataService,
    private readonly repoJobService: RepoJobService,
    private readonly userDataService: UserDataService,
    private readonly prismaService: PrismaService,
  ) {}

  async getRepoPaths(repo: Repo): Promise<{
    repoPath: string;
    dbPath: string;
    filesPath: string;
  }> {
    const group = await this.groupDataService.getGroup(repo.groupId);
    if (!group) {
      throw new NotFoundException();
    }

    const repoPath = `${group.name}/${repo.name}`;
    return {
      repoPath,
      dbPath: `${repoPath}/${group.name}-${repo.name}.db`,
      filesPath: `${repoPath}/${repo.name}.files.tar.gz`,
    };
  }

  async removeRepoDb(repo: Repo) {
    const { repoPath } = await this.getRepoPaths(repo);

    await this.s3.rm(`${repoPath}/`, {
      bucket: this.s3.reposBucket,
    });
  }

  async createRepoDb(repo: Repo): Promise<void> {
    const { repoPath, dbPath } = await this.getRepoPaths(repo);
    ////console.log('dbPath:', dbPath);

    const tarStream = pack();
    const gzipStream = tarStream.pipe(createGzip());
    tarStream.finalize();

    await this.s3.put(dbPath, gzipStream, {
      bucket: this.s3.reposBucket,
    });

    await this.updateIndexHtml(repoPath);

    //const output = await this.exec(`repo-add '${dbPath}'`);
    ////console.log('output:', output);

    //await this.removeOldFiles(repo);
  }

  async cleanRepo(
    repo: Repo,
  ): Promise<{ savedFiles: string[]; deletedFiles: string[] }> {
    const { repoPath, dbPath } = await this.getRepoPaths(repo);

    return Promise.all([
      this.readPackagesFromRepoFile(dbPath),
      fsp.readdir(repoPath),
    ]).then((results) => {
      const repoPackages = results[0];
      const packageFiles = results[1];

      ////console.log('repoPackages:', repoPackages);
      ////console.log('packageFiles:', packageFiles);

      const savedFiles: string[] = [];
      const deletedFiles: string[] = [];

      for (const pkgFile of packageFiles) {
        if (!pkgFile.endsWith('.pkg.tar.gz')) {
          // ignore other files
          continue;
        }

        if (repoPackages.includes(pkgFile)) {
          savedFiles.push(pkgFile);
          continue;
        }

        fs.unlinkSync(path.join(repoPath, pkgFile));

        deletedFiles.push(pkgFile);
      }

      return { savedFiles, deletedFiles };
    });
  }

  async refreshRepo(
    repoId: string,
    userId: string,
    reportProgress: (progess: {
      pkgsDone: number;
      pkgsTotal: number;
      percentDone: number;
    }) => void,
  ): Promise<void> {
    //reportProgress({ pkgsDone: 0, pkgsTotal: 0, percentDone: 0 });

    //console.log('refresh 1');

    const repo = await this.repoDataService.getRepo(repoId, userId);
    if (!repo) {
      throw new NotFoundException('invalid repoId');
    }

    const user = await this.userDataService.getUser(userId);
    if (!user) {
      throw new NotFoundException('invalid userId');
    }

    //console.log('refresh 2');

    const { repoPath, dbPath } = await this.getRepoPaths(repo);
    //console.log('dbPath:', dbPath);

    const dbStats = await this.s3.stat(dbPath, {
      bucket: this.s3.reposBucket,
    });
    //console.log('dbStats:', dbStats);

    if (dbStats) {
      await this.s3.rm(dbPath, {
        bucket: this.s3.reposBucket,
      });
      //console.log('deleted', dbPath);
    }

    const indexPath = `${repoPath}/index.html`;
    const indexStats = await this.s3.stat(indexPath, {
      bucket: this.s3.reposBucket,
    });
    if (indexStats) {
      await this.s3.rm(indexPath, {
        bucket: this.s3.reposBucket,
      });
    }

    await this.pkgDependDataService.deletePkgDependsByRepoId(repo.id);
    await this.pkgDataService.deletePkgsByRepoId(repo.id);

    const pkgFiles = (
      await this.s3.ls(repoPath, {
        bucket: this.s3.reposBucket,
      })
    ).filter((item) => item.path.endsWith('.pkg.tar.gz'));
    //console.log('pkgFiles:', pkgFiles);

    reportProgress({
      percentDone: 0,
      pkgsDone: 0,
      pkgsTotal: pkgFiles.length,
    });

    for (let i = 0; i < pkgFiles.length; i++) {
      const pkgFile = pkgFiles[i];
      //console.log('pkgFile:', pkgFile);

      const pkgInfo = await this.parserService.parsePkgInfo(
        await this.readPkgInfoFromPkgfile(
          await this.s3.getReadable(pkgFile.path, {
            bucket: this.s3.reposBucket,
          }),
        ),
      );

      pkgInfo.md5 = await this.generateHash(
        'md5',
        await this.s3.getReadable(pkgFile.path, {
          bucket: this.s3.reposBucket,
        }),
      );

      pkgInfo.sha256 = await this.generateHash(
        'sha256',
        await this.s3.getReadable(pkgFile.path, {
          bucket: this.s3.reposBucket,
        }),
      );

      const pkgFileStats = await this.s3.stat(pkgFile.path, {
        bucket: this.s3.reposBucket,
      });
      if (!pkgFileStats) {
        throw new Error(`could not get stats for ${pkgFile.path}`);
      }
      pkgInfo.sizeC = pkgFileStats.size;

      console.log('pkginfo:', pkgInfo);

      const pkg = await this.createPkgFromPkgInfo(repo.id, user.id, pkgInfo);
      //console.log('pkg', pkg);

      await this.addPkgToDbFile(dbPath, pkg);
      //console.log('pkg', pkg.name, 'added to', dbPath);

      reportProgress({
        pkgsDone: i + 1,
        percentDone: (100 / pkgFiles.length) * (i + 1),
        pkgsTotal: pkgFiles.length,
      });
    }

    await this.updateIndexHtml(repoPath);
  }

  async cleanupRepo(
    repoId: string,
    userId: string,
    reportProgress: (progess: {
      pkgsInDatabase: number;
      pkgsInFilesystem: number;
      pkgsDeleted: number;
      pkgsToDelete: number;
      percentDone: number;
    }) => void,
  ): Promise<void> {
    const repo = await this.repoDataService.getRepo(repoId, userId);
    if (!repo) {
      throw new NotFoundException('invalid repoId');
    }

    const user = await this.userDataService.getUser(userId);
    if (!user) {
      throw new NotFoundException('invalid userId');
    }

    const archs = await this.archDataService.getArchs();
    console.log('archs:', archs);

    const pkgs = await this.pkgDataService.getPkgsByRepoId(repo.id);
    console.log('pkgs in database:', pkgs.length);

    const { repoPath } = await this.getRepoPaths(repo);

    const filesInRepo = await this.s3.ls(repoPath, {
      bucket: this.s3.reposBucket,
    });
    console.log('total files in repo directory:', filesInRepo.length);

    const pkgFilesInRepo = filesInRepo.filter((item) =>
      item.path.endsWith('.pkg.tar.gz'),
    );
    console.log('pkg files in repo directory:', pkgFilesInRepo.length);

    let pkgsInFilesystemCounter = pkgFilesInRepo.length;
    let pkgsDeletedCounter = 0;
    const initialPkgsToDelete = pkgFilesInRepo.length - pkgs.length;
    let pkgsToDeleteCounter = initialPkgsToDelete;

    reportProgress({
      pkgsInDatabase: pkgs.length,
      pkgsInFilesystem: pkgsInFilesystemCounter,
      pkgsDeleted: pkgsDeletedCounter,
      pkgsToDelete: pkgsToDeleteCounter,
      percentDone: 0,
    });

    const pkgSet: Set<string> = new Set();
    for (const pkg of pkgs) {
      const arch = archs.find((item) => item.id === pkg.archId);
      if (!arch) {
        throw new Error(
          `pkg '${pkg.name}' has an invalid arch id: ${pkg.archId}`,
        );
      }
      const pkgFileName = this.getPkgFileName(
        pkg.name,
        pkg.ver,
        pkg.rel,
        arch.name,
      );

      pkgSet.add(`${repoPath}/${pkgFileName}`);
    }
    //console.log('pkgSet:', pkgSet);

    for (const pkgFile of pkgFilesInRepo) {
      //console.log('file:', pkgFile.path, 'date:', pkgFile.date);

      if (!pkgSet.has(pkgFile.path)) {
        console.log('delete file', pkgFile.path);
        await this.s3.rm(pkgFile.path, { bucket: this.s3.reposBucket });
        pkgsDeletedCounter++;
        pkgsToDeleteCounter--;
        pkgsInFilesystemCounter--;
      }

      reportProgress({
        pkgsInDatabase: pkgs.length,
        pkgsInFilesystem: pkgsInFilesystemCounter,
        pkgsDeleted: pkgsDeletedCounter,
        pkgsToDelete: pkgsToDeleteCounter,
        percentDone: (100 / initialPkgsToDelete) * pkgsDeletedCounter,
      });
    }

    await this.updateIndexHtml(repoPath);
  }

  async generateHash(
    hashType: 'md5' | 'sha256',
    readStream: Readable,
  ): Promise<string> {
    return new Promise<string>((resolve, reject) => {
      readStream
        .pipe(createHash(hashType).setEncoding('hex'))
        .on('data', (hash) => {
          resolve(hash);
        })
        .on('error', (error) => reject(error));
    });
  }

  async readPkgInfoFromPkgfile(readStream: Readable): Promise<string> {
    return new Promise<string>((resolve, reject) => {
      readStream.on('error', (error) => reject(error));

      const gunzipStream = createGunzip();
      gunzipStream.on('error', (error) => reject(error));

      const extractStream = extract();
      extractStream.on('error', (error) => reject(error));
      extractStream.on('entry', (header, stream, next) => {
        stream.on('error', (error) => reject(error));

        if (header.name === '.PKGINFO') {
          let pkgInfo = '';
          const textDecoder = new TextDecoder();

          stream.on('data', (chunk) => {
            pkgInfo += textDecoder.decode(chunk);
          });

          stream.on('end', () => {
            extractStream.destroy();
            gunzipStream.destroy();
            readStream.destroy();
            resolve(pkgInfo);
          });
        } else {
          stream.on('end', () => next());
          stream.resume();
        }
      });

      readStream.pipe(gunzipStream).pipe(extractStream);
    });
  }

  async readPackagesFromRepoFile(repoDbPath: string): Promise<string[]> {
    const packages = await this.gzipFileService.readContentFromGzipFile(
      repoDbPath,
      (header) => header.name.endsWith('/desc'),
    );

    return packages.map(
      (entry) => this.parserService.parsePkgDesc(entry.content).fileName,
    );
  }

  async addPkgToDbFile(dbPath: string, pkg: Pkg): Promise<void> {
    const { path: oldFilePath, cleanup: cleanupOldFile } = await tempFile();
    const { path: newFilePath, cleanup: cleanupNewFile } = await tempFile();
    //console.log('addPkgToDbFile created temp files:', oldFilePath, newFilePath);

    const pkgGroups = await this.prismaService.pkgGroup.findMany({
      where: { pkgs: { some: { id: pkg.id } } },
    });

    let dbExists = true;
    try {
      await this.s3.get(dbPath, fs.createWriteStream(oldFilePath), {
        bucket: this.s3.reposBucket,
      });
    } catch (error) {
      dbExists = false;
    }
    //console.log('addPkgToDbFile got old db file, exists:', dbExists);

    const tarExtract = extract();
    const tarPack = pack();

    const oldFileReadStream = fs.createReadStream(oldFilePath);
    const newFileWriteStream = fs.createWriteStream(newFilePath);

    return new Promise<void>((resolve, reject) => {
      //console.log('addPkgToDbFile new promise');

      oldFileReadStream.on('error', (error) => reject(error));

      tarExtract.on('entry', (headers, stream, callback) => {
        //console.log('addPkgToDbFile extract entry:', headers.name);

        const fileName = headers.name.split('-').slice(0, -2).join('-');
        //console.log('fileName:', fileName);

        if (fileName !== pkg.name) {
          //console.log('addPkgToDbFile copy stream');

          stream.pipe(tarPack.entry(headers, callback));
        } else {
          //console.log('addPkgToDbFile skip stream');
          stream.resume();
          callback();
        }
      });
      tarExtract.on('finish', async () => {
        //console.log('addPkgToDbFile extract finish');

        const dirName = `${pkg.name}-${pkg.ver}-${pkg.rel}`;
        //console.log('addPkgToDbFile add entry:', dirName);
        tarPack.entry({ name: dirName, type: 'directory' }, (error) => {
          //console.log('addPkgToDbFile pack dir entry done');

          if (error) {
            //console.log('entry dir error:', error);
          }
        });
        //console.log('addPkgToDbFile dir entry created');
        tarPack.entry(
          { name: `${dirName}/desc` },
          await this.parserService.createDescFromPkg(pkg, pkgGroups),
          (error) => {
            //console.log('addPkgToDbFile pack entry done');

            if (error) {
              //console.log('entry desc error:', error);
            }
            //console.log('tarPack finalize');
            tarPack.finalize();
          },
        );
      });
      tarExtract.on('error', (error) => {
        //console.log('addPkgToDbFile extract error');

        reject(error);
      });

      tarPack.on('error', (error) => {
        //console.log('addPkgToDbFile pack error');

        reject(error);
      });

      newFileWriteStream.on('error', (error) => {
        //console.log('addPkgToDbFile newFilestream error');

        reject(error);
      });
      newFileWriteStream.on('finish', async () => {
        //console.log('addPkgToDbFile newFilestream finish');

        await cleanupOldFile();
        //console.log('addPkgToDbFile cleaned old temp file');

        await this.s3.put(dbPath, fs.createReadStream(newFilePath), {
          bucket: this.s3.reposBucket,
        });
        //console.log('addPkgToDbFile uploaded db file:', dbPath);

        await cleanupNewFile();
        //console.log('addPkgToDbFile cleaned new temp file');

        //console.log('reolve');
        resolve();
      });

      tarPack.pipe(createGzip()).pipe(newFileWriteStream);
      //console.log('addPkgToDbFile tarPack pipe');

      if (dbExists) {
        oldFileReadStream.pipe(createGunzip()).pipe(tarExtract);
      } else {
        tarExtract.end();
      }
      //console.log('addPkgToDbFile oldFileReadStream pipe');
    });
  }

  async removePkgFromDatabase(pkgId: string) {
    await this.pkgDependDataService.deletePkgDependsByPkgId(pkgId);
    await this.pkgDataService.deletePkg(pkgId);
  }

  async removePkgFromDbFile(
    dbPath: string,
    pkgName: string,
  ): Promise<{ dbFileUpdated: boolean }> {
    const { path: currentDbFilePath, cleanup: cleanupCurrentDbFile } =
      await tempFile();
    const { path: newDbFilePath, cleanup: cleanupNewDbFile } = await tempFile();

    await this.s3.get(dbPath, fs.createWriteStream(currentDbFilePath), {
      bucket: this.s3.reposBucket,
    });

    const tarExtract = extract();
    const tarPack = pack();

    const currentDbReadStream = fs.createReadStream(currentDbFilePath);
    const newDbWriteStream = fs.createWriteStream(newDbFilePath);

    return new Promise<{ dbFileUpdated: boolean }>((resolve, reject) => {
      currentDbReadStream.on('error', (error) => reject(error));
      newDbWriteStream.on('error', (error) => reject(error));
      tarExtract.on('error', (error) => reject(error));
      tarPack.on('error', (error) => reject(error));

      let pkgFound = false;

      // current db file tar entry
      tarExtract.on('entry', (headers, stream, callback) => {
        const fileName = headers.name.split('-').slice(0, -2).join('-');
        //console.log('fileName:', fileName);
        if (fileName === pkgName) {
          // skip existing pkg
          pkgFound = true;
          stream.resume();
          callback();
        } else {
          stream.pipe(tarPack.entry(headers, callback));
        }
      });

      tarExtract.on('finish', async () => {
        tarPack.finalize();
      });

      newDbWriteStream.on('finish', async () => {
        await cleanupCurrentDbFile();

        if (pkgFound) {
          await this.s3.put(dbPath, fs.createReadStream(newDbFilePath), {
            bucket: this.s3.reposBucket,
          });
        }

        await cleanupNewDbFile();

        resolve({ dbFileUpdated: pkgFound });
      });

      // new db write pipeline
      tarPack.pipe(createGzip()).pipe(newDbWriteStream);

      // start reading current db
      currentDbReadStream.pipe(createGunzip()).pipe(tarExtract);
    });
  }

  async updateIndexHtml(repoPath: string): Promise<void> {
    const storageItems = await this.s3.ls(repoPath, {
      bucket: this.s3.reposBucket,
    });

    const html = minify(
      await this.templateService.render('repo-index/index.html', {
        repoPath,
        files: storageItems
          .map((item) => ({
            name: path.basename(item.path),
            etag: item.etag,
            date: item.date?.toLocaleString(),
            size: item.size,
          }))
          .filter(
            (item) =>
              item.name.endsWith('.pkg.tar.gz') || item.name.endsWith('.db'),
          ),
      }),
      { minifyCSS: true },
    );

    const repoFile = `${repoPath}/index.html`;
    await this.s3.put(repoFile, Readable.from([html]), {
      bucket: this.s3.reposBucket,
    });
    //this.logger.debug(`updated index file '${repoFile}'`);
  }

  async createPkgFromPkgInfo(
    repoId: string,
    userId: string,
    pkgInfo: PkgInfo,
  ): Promise<Pkg> {
    const repo = await this.repoDataService.getRepo(repoId, userId);
    if (!repo) {
      throw new NotFoundException();
    }

    const arch = await this.archDataService.getArchByName(pkgInfo.arch);
    if (!arch) {
      throw new NotFoundException(`arch '${pkgInfo.arch}' does not exist`);
    }

    let pkgName = await this.pkgNameDataService.getPkgNameByName(pkgInfo.name);
    if (!pkgName) {
      pkgName = await this.pkgNameDataService.createPkgName({
        name: pkgInfo.name,
      });
    }

    const existingPkg = await this.pkgDataService.getPkgByRepoIdAndPkgNameId(
      repo.id,
      pkgName.id,
      userId,
    );
    if (existingPkg) {
      // only remove it from database for now
      // TODO implement a regular running cleanup job
      await this.removePkgFromDatabase(existingPkg.id);
    }

    const pkg = await this.prismaService.pkg.create({
      data: {
        pkgNameId: pkgName.id,
        archId: arch.id,
        repoId: repoId,
        uploaderId: userId,
        name: pkgInfo.name,
        base: pkgInfo.base,
        desc: pkgInfo.desc,
        url: pkgInfo.url,
        license: pkgInfo.license,
        ver: pkgInfo.ver,
        rel: pkgInfo.rel,
        sizeC: pkgInfo.sizeC,
        sizeI: pkgInfo.sizeI,
        md5: pkgInfo.md5,
        sha256: pkgInfo.sha256,
        buildDate: pkgInfo.buildDate,
        uploadDate: new Date(),
        anyArch: pkgInfo.arch === 'any',
        packager: pkgInfo.packager,
        pkgGroups: {
          connectOrCreate: pkgInfo.groups.map((groupName) => ({
            where: { name: groupName },
            create: { name: groupName },
          })),
        },
        depends: {
          create: [
            ...[
              { type: PkgDependType.DEPEND, depends: pkgInfo.depends },
              { type: PkgDependType.MAKE, depends: pkgInfo.makeDepends },
              { type: PkgDependType.CHECK, depends: pkgInfo.checkDepends },
              { type: PkgDependType.OPT, depends: pkgInfo.optDepends },
              { type: PkgDependType.PROVIDE, depends: pkgInfo.provides },
              { type: PkgDependType.REPLACE, depends: pkgInfo.replaces },
              { type: PkgDependType.CONFLICT, depends: pkgInfo.conflicts },
            ]
              .map(({ type, depends }) => {
                return depends.map((depend) => ({
                  type,
                  pkgName: {
                    connectOrCreate: {
                      where: { name: depend.name },
                      create: { name: depend.name },
                    },
                  },
                  comparator: depend.comperator,
                  version: depend.version,
                  comment: depend.comment,
                }));
              })
              .flat(),
          ],
        },
      },
    });

    return pkg;
  }

  async deletePkg(
    pkgName: string,
    repo: Repo,
    user: MyUser,
  ): Promise<{ deletedPkg: Pkg | null; deletedInRepoFile: boolean }> {
    const pkg = await this.pkgDataService.getPkgByRepoIdAndPkgName(
      repo.id,
      pkgName,
      user.id,
    );

    if (pkg) {
      await this.removePkgFromDatabase(pkg.id);
    }

    const { dbPath, repoPath } = await this.getRepoPaths(repo);

    const { dbFileUpdated } = await this.removePkgFromDbFile(dbPath, pkgName);

    if (dbFileUpdated) {
      await this.updateIndexHtml(repoPath);
    }

    return { deletedPkg: pkg, deletedInRepoFile: dbFileUpdated };
  }

  getPkgFileName(name: string, ver: string, rel: number, arch: string): string {
    return `${name}-${ver}-${rel}-${arch}.pkg.tar.gz`;
  }

  async deleteRepo(repoId: string, userId: string): Promise<Job> {
    const repo = await this.repoDataService.getRepo(repoId, userId);
    if (!repo) {
      throw new NotFoundException('repo not found');
    }
    return this.repoJobService.addRepoDeleteJob({ repoId, userId });
  }
}
