import {
  IsAlphanumeric,
  IsLowercase,
  IsNotEmpty,
  IsString,
  MaxLength,
} from 'class-validator';

export class CreateRepoInput {
  @IsString()
  @IsNotEmpty()
  groupId!: string;

  @IsString()
  @IsNotEmpty()
  archId!: string;

  @IsNotEmpty()
  @IsAlphanumeric()
  @MaxLength(32)
  @IsLowercase()
  name!: string;
}

export class UpdateRepoInput {
  @IsNotEmpty()
  @IsAlphanumeric()
  @MaxLength(32)
  @IsLowercase()
  name?: string;
}

export class Repo {
  id!: string;

  groupId!: string;

  archId!: string;

  name!: string;

  createdAt!: Date;

  modifiedAt!: Date;
}
