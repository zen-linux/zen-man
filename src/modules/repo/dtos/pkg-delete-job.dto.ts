export class PkgDeleteJob {
  id!: string;
  state!: string;
  failedReason?: string;
  createdAt!: Date;
}
