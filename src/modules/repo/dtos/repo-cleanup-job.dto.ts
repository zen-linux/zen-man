import { IsNotEmpty, IsString } from 'class-validator';
import { RepoCleanupProgress } from './repo-cleanup-progress';

export class CreateRepoCleanupJobInput {
  @IsString()
  @IsNotEmpty()
  repoId!: string;
}

export class RepoCleanupJob {
  id!: string;
  state!: string;
  progress!: RepoCleanupProgress;
  createdAt!: Date;
}
