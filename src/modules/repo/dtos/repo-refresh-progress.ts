export class RepoRefreshProgress {
  pkgsDone!: number;
  pkgsTotal!: number;
  percentDone!: number;
}
