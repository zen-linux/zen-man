export class RepoCleanupProgress {
  pkgsInDatabase!: number;
  pkgsInFilesystem!: number;
  pkgsDeleted!: number;
  pkgsToDelete!: number;
  percentDone!: number;
}
