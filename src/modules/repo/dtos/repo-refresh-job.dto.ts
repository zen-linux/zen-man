import { IsNotEmpty, IsString } from 'class-validator';
import { RepoRefreshProgress } from './repo-refresh-progress';

export class CreateRepoRefreshJobInput {
  @IsString()
  @IsNotEmpty()
  repoId!: string;
}

export class RepoRefreshJob {
  id!: string;
  state!: string;
  progress!: RepoRefreshProgress;
  createdAt!: Date;
}
