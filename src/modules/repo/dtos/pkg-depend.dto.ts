import { PkgDependType } from '../constants';

export class CreatePkgDependInput {
  pkgId!: string;
  pkgNameId!: string;
  type!: PkgDependType;
  version?: string;
  comparator?: string;
  comment?: string;
}

export class PkgDepend {
  id!: string;

  pkgId!: string;

  pkgNameId!: string;

  type!: PkgDependType;

  version: string | null;

  comparator: string | null;

  comment: string | null;

  createdAt!: Date;

  modifiedAt!: Date;
}
