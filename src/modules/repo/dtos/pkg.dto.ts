import { IsNotEmpty, IsString } from 'class-validator';

export class CreatePkgInput {
  @IsString()
  @IsNotEmpty()
  repoId!: string;
}

export class CreatePkgInputInternal {
  pkgNameId!: string;

  archId!: string;

  repoId!: string;

  uploaderId!: string;

  name!: string;

  base!: string;

  desc!: string;

  url!: string;

  license!: string;

  ver!: string;

  rel!: number;

  sizeC!: number;

  sizeI!: number;

  anyArch!: boolean;

  md5!: string;

  sha256!: string;

  buildDate!: Date;

  packager!: string;

  uploadDate!: Date;
}

export class Pkg {
  id!: string;

  pkgNameId!: string;

  archId!: string;

  repoId!: string;

  uploaderId!: string;

  name!: string;

  base!: string;

  desc!: string;

  url!: string;

  license!: string;

  ver!: string;

  rel!: number;

  sizeC!: number;

  sizeI!: number;

  anyArch!: boolean;

  md5!: string;

  sha256!: string;

  buildDate!: Date;

  packager!: string;

  uploadDate!: Date;

  createdAt!: Date;

  modifiedAt!: Date;
}
