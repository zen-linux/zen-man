import { IsAlphanumeric, IsLowercase, IsNotEmpty } from 'class-validator';

export class CreatePkgNameInput {
  @IsNotEmpty()
  @IsAlphanumeric()
  @IsLowercase()
  name!: string;
}

export class UpdatePkgNameInput {
  @IsNotEmpty()
  @IsAlphanumeric()
  @IsLowercase()
  name?: string;
}

export class PkgName {
  id!: string;

  name!: string;

  createdAt!: Date;

  modifiedAt!: Date;
}
