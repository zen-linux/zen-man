export class PkgUploadJob {
  id!: string;
  state!: string;
  failedReason?: string;
  createdAt!: Date;
}
