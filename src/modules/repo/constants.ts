export const enum PkgDependType {
  DEPEND,
  MAKE,
  CHECK,
  OPT,
  PROVIDE,
  CONFLICT,
  REPLACE,
}

export const PKG_UPLOAD_QUEUE = 'pkg-upload';
export const PKG_DELETE_QUEUE = 'pkg-delete';

export const REPO_DELETE_QUEUE = 'repo-delete';
export const REPO_REFRESH_QUEUE = 'repo-refresh';
export const REPO_CLEANUP_QUEUE = 'repo-cleanup';
