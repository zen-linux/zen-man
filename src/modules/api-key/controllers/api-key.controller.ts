import { AuthUserId } from '@core/decorators/auth-user-id.decorator';
import { DeleteInfo } from '@modules/data/dtos/delete-info.dto';
import { ApiKeyDataService } from '@modules/data/services/api-key-data.service';
import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Post,
  Put,
} from '@nestjs/common';
import {
  ApiKey,
  CreateApiKeyInput,
  UpdateApiKeyInput,
} from '../dtos/api-key.dto';

@Controller({
  path: 'api-keys',
  version: '1',
})
export class ApiKeyController {
  constructor(private readonly apiKeyDataService: ApiKeyDataService) {}

  @Get()
  getApiKeys(@AuthUserId() userId: string): Promise<ApiKey[]> {
    return this.apiKeyDataService.getApiKeys(userId);
  }

  @Get(':id')
  getApiKey(
    @Param('id') id: string,
    @AuthUserId() userId: string,
  ): Promise<ApiKey | null> {
    return this.apiKeyDataService.getApiKey(id, userId);
  }

  @Post()
  createApiKey(
    @Body() data: CreateApiKeyInput,
    @AuthUserId() userId: string,
  ): Promise<ApiKey> {
    return this.apiKeyDataService.createApiKey({
      description: data.description,
      userId,
    });
  }

  @Put()
  updateApiKey(
    @Param('id') id: string,
    @Body() data: UpdateApiKeyInput,
    @AuthUserId() userId: string,
  ): Promise<ApiKey> {
    return this.apiKeyDataService.updateApiKey(id, data, userId);
  }

  @Delete(':id')
  deleteApiKey(
    @Param('id') id: string,
    @AuthUserId() userId: string,
  ): Promise<ApiKey> {
    return this.apiKeyDataService.deleteApiKey(id, userId);
  }

  @Delete()
  deleteApiKeys(@AuthUserId() userId: string): Promise<DeleteInfo> {
    return this.apiKeyDataService.deleteApiKeys(userId);
  }
}
