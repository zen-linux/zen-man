import { IsNotEmpty, IsString, MaxLength } from 'class-validator';

export class CreateApiKeyInput {
  @IsString()
  @IsNotEmpty()
  @MaxLength(50)
  description!: string;
}

export class CreateApiKeyInputInternal {
  userId!: string;

  description!: string;
}

export class UpdateApiKeyInput {
  @IsString()
  @IsNotEmpty()
  @MaxLength(50)
  description!: string;
}

export class ApiKey {
  id!: string;

  userId!: string;

  description!: string;

  lastUsed: Date | null;

  createdAt!: Date;

  modifiedAt!: Date;
}

export class ApiKeyWithKey extends ApiKey {
  key!: string;
}
