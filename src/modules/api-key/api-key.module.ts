import { AuthModule } from '@modules/auth/auth.module';
import { CaslModule } from '@modules/casl/casl.module';
import { UserModule } from '@modules/user/user.module';
import { Module } from '@nestjs/common';
import { ApiKeyController } from './controllers/api-key.controller';

@Module({
  controllers: [ApiKeyController],
  imports: [AuthModule, UserModule, CaslModule],
  providers: [],
  exports: [],
})
export class ApiKeyModule {}
