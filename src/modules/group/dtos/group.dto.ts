import { USER_NAME_MAX_LENGTH } from '@modules/user/dtos/user.dto';
import { IsNotEmpty, IsString, MaxLength } from 'class-validator';

export class CreateGroupInput {
  @IsString()
  @IsNotEmpty()
  @MaxLength(USER_NAME_MAX_LENGTH)
  name!: string;

  @IsString()
  @IsNotEmpty()
  @MaxLength(50)
  description!: string;
}

export class UpdateGroupInput {
  @IsString()
  @MaxLength(USER_NAME_MAX_LENGTH)
  name?: string;

  @IsString()
  @MaxLength(50)
  description?: string;
}

export class Group {
  id!: string;

  name!: string;

  description!: string;

  createdAt!: Date;

  modifiedAt!: Date;
}
