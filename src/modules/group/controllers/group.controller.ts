import { AuthUserId } from '@core/decorators/auth-user-id.decorator';
import { GroupDataService } from '@modules/data/services/group-data.service';
import { MemberDataService } from '@modules/data/services/member-data.service';
import { Member } from '@modules/member/dtos/member.dto';
import {
  Body,
  Controller,
  Delete,
  Get,
  NotFoundException,
  Param,
  Post,
} from '@nestjs/common';
import { CreateGroupInput, Group } from '../dtos/group.dto';

@Controller({
  path: 'groups',
  version: '1',
})
export class GroupController {
  constructor(
    private readonly groupDataService: GroupDataService,
    private readonly memberDataService: MemberDataService,
  ) {}

  @Get()
  getGroups(@AuthUserId() userId: string): Promise<Group[]> {
    return this.groupDataService.getGroups(userId);
  }

  @Get('name/:name')
  async getGroupByName(
    @Param('name') name: string,
    @AuthUserId() userId: string,
  ): Promise<Group> {
    const group = await this.groupDataService.getGroupByName(name, userId);
    if (!group) {
      throw new NotFoundException();
    }
    return group;
  }

  @Get(':id')
  async getGroup(
    @Param('id') id: string,
    @AuthUserId() userId: string,
  ): Promise<Group> {
    const group = await this.groupDataService.getGroup(id, userId);
    if (!group) {
      throw new NotFoundException();
    }
    return group;
  }

  @Get(':groupId/members')
  async getGroupMembers(
    @Param('id') id: string,
    @AuthUserId() userId: string,
  ): Promise<Member[]> {
    const group = await this.groupDataService.getGroup(id, userId);
    if (!group) {
      throw new NotFoundException();
    }
    return this.memberDataService.getMembersForGroupId(group.id);
  }

  // api/v1/groups/name/zen/repos/name/core
  //
  // api/v1/groups/id
  //
  // api/v1/repos/name/zen/core

  @Post()
  async createGroup(
    @Body() data: CreateGroupInput,
    @AuthUserId() userId: string,
  ): Promise<Group> {
    const group = await this.groupDataService.createGroup(data);
    await this.memberDataService.createMember({
      groupId: group.id,
      userId,
      isAdmin: true,
    });
    return group;
  }

  @Delete(':id')
  deleteGroup(
    @Param('id') id: string,
    @AuthUserId() userId: string,
  ): Promise<Group> {
    return this.groupDataService.deleteGroup(id, userId);
  }
}
