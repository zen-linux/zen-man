import { AuthUserId } from '@core/decorators/auth-user-id.decorator';
import { MulterFile } from '@core/models/multer-file';
import { IsoDataService } from '@modules/data/services/iso-data.service';
import {
  Body,
  Controller,
  Delete,
  Get,
  NotFoundException,
  Param,
  Post,
  UploadedFile,
  UseInterceptors,
} from '@nestjs/common';
import { FileInterceptor } from '@nestjs/platform-express';
import { CreateIsoInput, IsoWithUrl } from '../dtos/iso.dto';
import { IsoService } from '../services/iso.service';

@Controller({
  path: 'isos',
  version: '1',
})
export class IsoController {
  constructor(
    private readonly isoDataService: IsoDataService,
    private readonly isoService: IsoService,
  ) {}

  @Get()
  async getIsos(): Promise<IsoWithUrl[]> {
    const isos = await this.isoDataService.getIsos();
    return isos.map((iso) => this.isoService.appendUrl(iso));
  }

  @Get(':id')
  async getIso(@Param('id') id: string): Promise<IsoWithUrl> {
    const iso = await this.isoDataService.getIso(id);
    if (!iso) {
      throw new NotFoundException();
    }
    return this.isoService.appendUrl(iso);
  }

  @Post()
  @UseInterceptors(FileInterceptor('iso'))
  async uploadIso(
    @Body() data: CreateIsoInput,
    @UploadedFile() iso: MulterFile,
    @AuthUserId() userId: string,
  ): Promise<IsoWithUrl> {
    //console.log('iso:', iso);
    return this.isoService.appendUrl(
      await this.isoService.createIso(data, iso, userId),
    );
  }

  @Delete(':id')
  async deleteIso(
    @Param('id') id: string,
    @AuthUserId() userId: string,
  ): Promise<IsoWithUrl> {
    return this.isoService.appendUrl(
      await this.isoService.deleteIso(id, userId),
    );
  }
}
