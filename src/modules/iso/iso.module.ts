import { s3StorageEngine } from '@modules/s3/s3-storage-engine';
import { S3Module } from '@modules/s3/s3.module';
import { S3Service } from '@modules/s3/services/s3.service';
import { SettingsModule } from '@modules/settings/settings.module';
import { Module } from '@nestjs/common';
import { MulterModule } from '@nestjs/platform-express';
import { MulterOptions } from '@nestjs/platform-express/multer/interfaces/multer-options.interface';
import { IsoController } from './controllers/iso.controller';
import { IsoService } from './services/iso.service';

@Module({
  imports: [
    MulterModule.registerAsync({
      imports: [S3Module],
      inject: [S3Service],
      useFactory: (s3: S3Service) => {
        const multerOptions: MulterOptions = {
          storage: s3StorageEngine({
            s3Client: s3.client,
            bucket: s3.assetsBucket,
          }),
        };
        return multerOptions;
      },
    }),
    S3Module,
    SettingsModule,
  ],
  controllers: [IsoController],
  providers: [IsoService],
  exports: [IsoService],
})
export class IsoModule {}
