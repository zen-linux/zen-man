import { SettingsService } from '@core/abstracts/settings.service';
import { MulterFile } from '@core/models/multer-file';
import { ArchDataService } from '@modules/data/services/arch-data.service';
import { IsoDataService } from '@modules/data/services/iso-data.service';
import { S3Service } from '@modules/s3/services/s3.service';
import { Injectable, UnprocessableEntityException } from '@nestjs/common';
import { CreateIsoInput, Iso, IsoWithUrl } from '../dtos/iso.dto';

@Injectable()
export class IsoService {
  private readonly assetsUrl: string;

  constructor(
    private readonly isoDataService: IsoDataService,
    private readonly archDataService: ArchDataService,
    private readonly s3: S3Service,
    readonly settingsService: SettingsService,
  ) {
    this.assetsUrl = settingsService.getAssetsUrl();
  }

  async createIso(
    data: CreateIsoInput,
    iso: MulterFile,
    userId: string,
  ): Promise<Iso> {
    const arch = await this.archDataService.getArch(data.archId);
    if (!arch) {
      throw new UnprocessableEntityException('invalid archId');
    }

    const now = new Date();
    const isoName = `zenlinux-${arch.name}-${now.getFullYear()}${now
      .getMonth()
      .toString()
      .padStart(2, '0')}${now.getDate().toString().padStart(2, '0')}${now
      .getHours()
      .toString()
      .padStart(2, '0')}${now.getMinutes().toString().padStart(2, '0')}.iso`;

    const isoDestPath = `iso/${arch.name}/${isoName}`;
    //console.log('isoDestPath:', isoDestPath);

    await this.s3.mv(iso.path, isoDestPath, {
      bucket: this.s3.assetsBucket,
      metaData: {
        md5: iso.md5,
        sha256: iso.sha256,
      },
      public: true,
    });

    return this.isoDataService.createIso(
      { archId: arch.id, name: isoName, path: isoDestPath },
      userId,
    );
  }

  async deleteIso(id: string, userId: string): Promise<Iso> {
    const iso = await this.isoDataService.deleteIso(id, userId);
    await this.s3.rm(iso.path, { bucket: this.s3.assetsBucket });
    return iso;
  }

  appendUrl(iso: Iso): IsoWithUrl {
    return {
      id: iso.id,
      archId: iso.archId,
      name: iso.name,
      url: `${this.assetsUrl}/${iso.path}`,
      createdAt: iso.createdAt,
      modifiedAt: iso.modifiedAt,
    };
  }
}
