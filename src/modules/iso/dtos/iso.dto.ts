import { IsNotEmpty, IsString } from 'class-validator';

export class CreateIsoInput {
  @IsString()
  @IsNotEmpty()
  archId!: string;
}

export class CreateIsoInputInternal {
  archId!: string;
  name!: string;
  path!: string;
}

export class Iso {
  id!: string;

  archId!: string;

  name!: string;

  path!: string;

  createdAt!: Date;

  modifiedAt!: Date;
}

export class IsoWithUrl {
  id!: string;

  archId!: string;

  name!: string;

  url!: string;

  createdAt!: Date;

  modifiedAt!: Date;
}
