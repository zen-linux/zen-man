import { TemplateService } from '@core/abstracts/template.service';
import { HandlebarsTemplateModule } from '@frameworks/template/handlebars/handlebars-template.module';
import { HandlebarsTemplateService } from '@frameworks/template/handlebars/services/handlebars-template.service';
import { Module } from '@nestjs/common';

@Module({
  imports: [HandlebarsTemplateModule],
  providers: [
    {
      provide: TemplateService,
      useExisting: HandlebarsTemplateService,
    },
  ],
  exports: [TemplateService],
})
export class TemplateModule {}
