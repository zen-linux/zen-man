import { AuthUserId } from '@core/decorators/auth-user-id.decorator';
import { DeleteInfo } from '@modules/data/dtos/delete-info.dto';
import { ArchDataService } from '@modules/data/services/arch-data.service';
import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Post,
  Put,
} from '@nestjs/common';
import { Arch, CreateArchInput, UpdateArchInput } from '../dtos/arch.dto';

@Controller({
  path: 'archs',
  version: '1',
})
export class ArchController {
  constructor(private readonly archDataService: ArchDataService) {}

  @Get()
  getArchs(): Promise<Arch[]> {
    return this.archDataService.getArchs();
  }

  @Get(':id')
  getArch(@Param('id') id: string): Promise<Arch | null> {
    return this.archDataService.getArch(id);
  }

  @Post()
  createArch(
    @Body() data: CreateArchInput,
    @AuthUserId() userId: string,
  ): Promise<Arch> {
    return this.archDataService.createArch(data, userId);
  }

  @Put(':id')
  updateArch(
    @Param('id') id: string,
    @Body() data: UpdateArchInput,
    @AuthUserId() userId: string,
  ): Promise<Arch> {
    return this.archDataService.updateArch(id, data, userId);
  }

  @Delete(':id')
  deleteArch(
    @Param('id') id: string,
    @AuthUserId() userId: string,
  ): Promise<Arch> {
    return this.archDataService.deleteArch(id, userId);
  }

  @Delete()
  deleteArchs(@AuthUserId() userId: string): Promise<DeleteInfo> {
    return this.archDataService.deleteArchs(userId);
  }
}
