import { Module } from '@nestjs/common';
import { ArchController } from './controllers/arch.controller';

@Module({
  controllers: [ArchController],
  providers: [],
  exports: [],
})
export class ArchModule {}
