import { IsNotEmpty, IsString, MaxLength } from 'class-validator';

export class CreateArchInput {
  @IsString()
  @IsNotEmpty()
  @MaxLength(16)
  name!: string;
}

export class UpdateArchInput {
  @IsString()
  @IsNotEmpty()
  @MaxLength(16)
  name!: string;
}

export class Arch {
  id!: string;

  name!: string;

  createdAt!: Date;

  modifiedAt!: Date;
}
