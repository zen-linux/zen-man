import { Injectable } from '@nestjs/common';
import { createReadStream } from 'fs';
import { Readable } from 'stream';
import { StringDecoder } from 'string_decoder';
import * as tarStream from 'tar-stream';
import * as zlib from 'zlib';

@Injectable()
export class GzipService {
  async getInstalledSize(path: string): Promise<number> {
    return new Promise<number>((resolve, reject) => {
      let installedSize = 0;
      createReadStream(path)
        .on('error', (error) => reject(error))
        .pipe(zlib.createGunzip())
        .on('error', (error) => reject(error))
        .pipe(tarStream.extract())
        .on('error', (error) => reject(error))
        .on('entry', (header, stream, next) => {
          if (
            header.size !== undefined &&
            header.size !== 0 &&
            header.name !== '.PKGINFO' &&
            header.name !== '.BUILDINFO' &&
            header.name !== '.MTREE'
          ) {
            installedSize += header.size;
          }
          stream.on('end', () => next());
          stream.resume();
        })
        .on('finish', () => {
          resolve(installedSize);
        });
    });
  }

  async readContentFromGzipFile(
    gzipFilePath: string,
    filter: (header: tarStream.Headers) => boolean,
  ): Promise<{ path: string; content: string }[]> {
    return new Promise((resolve, reject) => {
      const content: { path: string; content: string }[] = [];

      try {
        const readStream = createReadStream(gzipFilePath);
        readStream.on('error', (error) => {
          reject(new Error(`stream error: ${error.message}`));
        });

        const gunzip = zlib.createGunzip();
        gunzip.on('error', (error) => {
          reject(new Error(`gzip error: ${error.message}`));
        });

        const tarExtract = tarStream.extract();
        tarExtract.on('entry', (header, stream, next) => {
          stream.resume();

          if (filter(header)) {
            const decoder = new StringDecoder();

            stream.on('data', (chunk: Buffer) => {
              const text = decoder.write(chunk);
              content.push({ path: header.name, content: text });
              //content += text;

              stream.resume();
            });

            stream.on('end', () => {
              next();
            });
          } else {
            next();
          }
        });
        tarExtract.on('finish', () => {
          resolve(content);
        });

        readStream.pipe(gunzip).pipe(tarExtract);
      } catch (error) {
        reject(error);
      }
    });
  }

  async readFileContentFromGzipStream(
    gzipStream: Readable,
    fileName: string,
  ): Promise<string> {
    return new Promise((resolve, reject) => {
      let content = '';

      try {
        gzipStream.on('error', (error) => {
          reject(new Error(`stream error: ${error.message}`));
        });

        const gunzip = zlib.createGunzip();

        gunzip.on('error', (error) => {
          reject(new Error(`gzip error: ${error.message}`));
        });

        const tarExtract = tarStream.extract();

        tarExtract.on('entry', (header, stream, next) => {
          console.log('tar entry:', header.name);

          if (header.name === fileName) {
            const decoder = new StringDecoder();

            stream.on('data', (chunk: Buffer) => {
              const text = decoder.write(chunk);
              content += text;
            });

            stream.on('end', () => {
              console.log('finished reading', fileName);
              stream.destroy();
              tarExtract.destroy();
              gzipStream.destroy();

              resolve(content);
            });
          } else {
            stream.resume();
            next();
          }
        });

        tarExtract.on('finish', () => {
          reject(new Error(`file not found in tar: ${fileName}`));
        });

        gzipStream.on('close', () => {
          console.log('gzipStream closed');
        });

        gzipStream.on('end', () => {
          console.log('gzipStream ended');
        });

        gzipStream.pipe(gunzip).pipe(tarExtract);
      } catch (error: unknown) {
        reject(error);
      }
    });
  }
}
