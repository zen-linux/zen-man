import { Module } from '@nestjs/common';
import { GzipService } from './services/gzip.service';

@Module({
  providers: [GzipService],
  exports: [GzipService],
})
export class GzipModule {}
