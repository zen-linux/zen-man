import { Module } from '@nestjs/common';
import { ParserService } from './services/parser.service';

@Module({
  providers: [ParserService],
  exports: [ParserService],
})
export class ParserModule {}
