// TODO use global @modules path again
import { Pkg } from '@modules/repo/dtos/pkg.dto';
import { Injectable, NotFoundException } from '@nestjs/common';
import { PkgGroup } from '@prisma/client';
import { ArchDataService } from '../../data/services/arch-data.service';
import { PkgDependDataService } from '../../data/services/pkg-depend-data.service';
import { PkgNameDataService } from '../../data/services/pkg-name-data.service';
import { PkgDependType } from '../../repo/constants';
import { PkgDesc } from '../models/pkg-desc';
import { PkgInfo, PkgInfoDepend } from '../models/pkg-info';

@Injectable()
export class ParserService {
  constructor(
    private readonly archDataservice: ArchDataService,
    private readonly pkgNameDataService: PkgNameDataService,
    private readonly pkgDependDataService: PkgDependDataService,
  ) {}

  parsePkgDesc(data: string): PkgDesc {
    const pkgDesc: PkgDesc = {
      arch: '',
      base: '',
      buildDate: new Date(),
      cSize: 0,
      depends: [],
      desc: '',
      fileName: '',
      iSize: 0,
      license: '',
      makeDepends: [],
      md5: '',
      name: '',
      optDepends: [],
      packager: '',
      rel: 0,
      sha256: '',
      url: '',
      ver: '',
    };

    const regex = /%([^\n]+)%\n([^\n]+|(?:[^\n]+\n[^\n]+)*)\n\n/gms;

    let m: RegExpExecArray | null;
    while ((m = regex.exec(data)) !== null) {
      // This is necessary to avoid infinite loops with zero-width matches
      if (m.index === regex.lastIndex) {
        regex.lastIndex++;
      }

      // The result can be accessed through the `m`-variable.

      const key = m[1];
      ////console.log('key:', key);

      const value = m[2];
      ////console.log('value:', value);

      switch (key) {
        case 'FILENAME':
          pkgDesc.fileName = value;
          break;
        case 'NAME':
          pkgDesc.name = value;
          break;
        case 'BASE':
          pkgDesc.base = value;
          break;
        case 'VERSION': {
          const verParts = value.split('-');
          pkgDesc.ver = verParts[0];
          pkgDesc.rel = Number.parseInt(verParts[1]);
          break;
        }
        case 'ARCH':
          pkgDesc.arch = value;
          break;
        case 'DESC':
          pkgDesc.desc = value;
          break;
        case 'URL':
          pkgDesc.url = value;
          break;
        case 'MD5SUM':
          pkgDesc.md5 = value;
          break;
        case 'SHA256SUM':
          pkgDesc.sha256 = value;
          break;
        case 'CSIZE':
          pkgDesc.cSize = Number.parseInt(value);
          break;
        case 'ISIZE':
          pkgDesc.iSize = Number.parseInt(value);
          break;
        case 'LICENSE':
          pkgDesc.license = value;
          break;
        case 'BUILDDATE':
          pkgDesc.buildDate = new Date(Number.parseInt(value) * 1000);
          break;
        case 'PACKAGER':
          pkgDesc.packager = value;
          break;
        case 'DEPENDS':
          pkgDesc.depends = value.split('\n');
          break;
        case 'MAKEDEPENDS':
          pkgDesc.makeDepends = value.split('\n');
          break;
      }
    }

    return pkgDesc;
  }

  parsePkgInfo(data: string): PkgInfo {
    const pkgInfo: PkgInfo = {
      arch: '',
      base: '',
      buildDate: new Date(),
      depends: [],
      desc: '',
      groups: [],
      license: '',
      makeDepends: [],
      checkDepends: [],
      md5: '',
      name: '',
      packager: '',
      rel: 0,
      sha256: '',
      sizeC: 0,
      sizeI: 0,
      url: '',
      ver: '',
      backups: [],
      optDepends: [],
      provides: [],
      replaces: [],
      conflicts: [],
    };

    const lines = data.split('\n');
    for (const line of lines) {
      if (line.startsWith('#') || line === '') {
        continue;
      }
      const lineParts = line.split(' = ');
      const key = lineParts[0].trim();

      switch (key) {
        case 'pkgname':
          pkgInfo.name = lineParts[1].trim();
          break;
        case 'pkgbase':
          pkgInfo.base = lineParts[1].trim();
          break;
        case 'pkgdesc':
          pkgInfo.desc = lineParts[1].trim();
          break;
        case 'group':
          pkgInfo.groups.push(lineParts[1].trim());
          break;
        case 'backup':
          pkgInfo.backups.push(lineParts[1].trim());
          break;
        case 'url':
          pkgInfo.url = lineParts[1].trim();
          break;
        case 'license':
          pkgInfo.license = lineParts[1].trim();
          break;
        case 'arch':
          pkgInfo.arch = lineParts[1].trim();
          break;
        case 'pkgver': {
          const verParts = lineParts[1].split('-');
          pkgInfo.ver = verParts[0];
          pkgInfo.rel = Number.parseInt(verParts[1].trim());
          break;
        }
        case 'size':
          pkgInfo.sizeI = Number.parseInt(lineParts[1].trim());
          break;
        case 'builddate':
          pkgInfo.buildDate = new Date(
            Number.parseInt(lineParts[1].trim()) * 1000,
          );
          break;
        case 'packager':
          pkgInfo.packager = lineParts[1].trim();
          break;
        case 'depend':
          this.pushToDepends(
            pkgInfo.depends,
            lineParts[1].trim(),
            PkgDependType.DEPEND,
          );
          break;
        case 'makedepend':
          this.pushToDepends(
            pkgInfo.makeDepends,
            lineParts[1].trim(),
            PkgDependType.MAKE,
          );
          break;
        case 'checkdepend':
          this.pushToDepends(
            pkgInfo.checkDepends,
            lineParts[1].trim(),
            PkgDependType.CHECK,
          );
          break;
        case 'optdepend':
          this.pushToDepends(
            pkgInfo.optDepends,
            lineParts[1].trim(),
            PkgDependType.OPT,
          );
          break;
        case 'provides':
          this.pushToDepends(
            pkgInfo.provides,
            lineParts[1].trim(),
            PkgDependType.PROVIDE,
          );
          break;
        case 'replaces':
          this.pushToDepends(
            pkgInfo.replaces,
            lineParts[1].trim(),
            PkgDependType.REPLACE,
          );
          break;
        case 'conflict':
          this.pushToDepends(
            pkgInfo.conflicts,
            lineParts[1].trim(),
            PkgDependType.CONFLICT,
          );
          break;
        default:
          console.log('unhandled key:', key, 'value:', lineParts[1]);
      }
    }

    return pkgInfo;
  }

  pushToDepends(
    dependsArray: PkgInfoDepend[],
    dependLine: string,
    pkgDependType: PkgDependType,
  ) {
    const depend = this.createPkgInfoDepend(dependLine, pkgDependType);
    if (
      dependsArray.findIndex(
        (item) =>
          item.name === depend.name && item.comperator === depend.comperator,
      ) === -1
    ) {
      dependsArray.push(depend);
    }
  }

  createPkgInfoDepend(
    dependLine: string,
    pkgDependType: PkgDependType,
  ): PkgInfoDepend {
    const parsedDependLine =
      pkgDependType === PkgDependType.OPT
        ? this.parseOptDependLine(dependLine)
        : this.parseDependLine(dependLine);

    const pkgInfoDepend: PkgInfoDepend = {
      name: parsedDependLine.name,
      type: pkgDependType,
    };

    if (parsedDependLine.version !== undefined) {
      pkgInfoDepend.version = parsedDependLine.version;
    }

    if (parsedDependLine.comment !== undefined) {
      pkgInfoDepend.comment = parsedDependLine.comment;
    }

    if (parsedDependLine.comperator !== undefined) {
      pkgInfoDepend.comperator = parsedDependLine.comperator;
    }

    return pkgInfoDepend;
  }

  parseDependLine(dependLine: string): {
    name: string;
    comperator?: string;
    version?: string;
    comment?: string;
  } {
    let comment: string | undefined;

    const regex = /\s*([^\s<>=]+)?\s*([<>=]{1,2})?\s*([^\s<>=]*)?\s*/;

    const matches = regex.exec(dependLine);

    if (matches === null) {
      throw new Error('regex failed');
    }

    const name = matches[1];
    const comperator = matches[2];
    const version = matches[3];

    const parsedLine: {
      name: string;
      comperator?: string;
      version?: string;
      comment?: string;
    } = {
      name: name,
    };
    if (comperator !== undefined) {
      parsedLine.comperator = comperator;
    }
    if (version !== undefined) {
      parsedLine.version = version;
    }
    if (comment !== undefined) {
      parsedLine.comment = comment;
    }

    return parsedLine;
  }

  parseOptDependLine(dependLine: string): {
    name: string;
    comperator?: string;
    version?: string;
    comment?: string;
  } {
    const [name, ...comments] = dependLine.split(':');
    const comment = comments.join(':').trim();

    const parsedLine: {
      name: string;
      comperator?: string;
      version?: string;
      comment?: string;
    } = {
      name: name,
    };

    if (comment !== '') {
      parsedLine.comment = comment;
    }

    return parsedLine;
  }

  async createDescFromPkg(pkg: Pkg, pkgGroups: PkgGroup[]): Promise<string> {
    const [
      pkgName,
      arch,
      depends,
      makeDepends,
      checkDepends,
      optDepends,
      provides,
      conflicts,
      replaces,
    ] = await Promise.all([
      this.pkgNameDataService.getPkgName(pkg.pkgNameId),
      this.archDataservice.getArch(pkg.archId),
      this.pkgDependDataService.getPkgDependsByPkgIdAndType(
        pkg.id,
        PkgDependType.DEPEND,
      ),
      this.pkgDependDataService.getPkgDependsByPkgIdAndType(
        pkg.id,
        PkgDependType.MAKE,
      ),
      this.pkgDependDataService.getPkgDependsByPkgIdAndType(
        pkg.id,
        PkgDependType.CHECK,
      ),
      this.pkgDependDataService.getPkgDependsByPkgIdAndType(
        pkg.id,
        PkgDependType.OPT,
      ),
      this.pkgDependDataService.getPkgDependsByPkgIdAndType(
        pkg.id,
        PkgDependType.PROVIDE,
      ),
      this.pkgDependDataService.getPkgDependsByPkgIdAndType(
        pkg.id,
        PkgDependType.CONFLICT,
      ),
      this.pkgDependDataService.getPkgDependsByPkgIdAndType(
        pkg.id,
        PkgDependType.REPLACE,
      ),
    ]);

    if (!pkgName || !arch) {
      throw new NotFoundException();
    }

    let desc = `%FILENAME%
${pkgName.name}-${pkg.ver}-${pkg.rel}-${arch.name}.pkg.tar.gz

%NAME%
${pkgName.name}

%BASE%
${pkg.base}

%VERSION%
${pkg.ver}-${pkg.rel}

%DESC%
${pkg.desc}
${
  pkgGroups.length > 0
    ? '\n%GROUPS%\n' +
      pkgGroups
        .map((group) => group.name)
        .sort()
        .join('\n') +
      '\n'
    : ''
}
%CSIZE%
${pkg.sizeC}

%ISIZE%
${pkg.sizeI}

%MD5SUM%
${pkg.md5}

%SHA256SUM%
${pkg.sha256}

%URL%
${pkg.url}

%LICENSE%
${pkg.license}

%ARCH%
${arch.name}

%BUILDDATE%
${Math.trunc(pkg.buildDate.getTime() / 1000)}

%PACKAGER%
${pkg.packager}
`;

    desc += this.addDepends('DEPENDS', depends);
    desc += this.addDepends('MAKEDEPENDS', makeDepends);
    desc += this.addDepends('CHECKDEPENDS', checkDepends);
    desc += this.addDepends('OPTDEPENDS', optDepends);
    desc += this.addDepends('PROVIDES', provides);
    desc += this.addDepends('CONFLICTS', conflicts);
    desc += this.addDepends('REPLACES', replaces);

    //desc = desc.trim();

    //console.log(desc.trim());
    return desc;
  }

  private addDepends(
    section: string,
    depends: {
      name: string;
      version?: string;
      comparator?: string;
      comment?: string;
    }[],
  ): string {
    if (depends.length > 0) {
      return `\n%${section}%\n${depends
        .map((item) => {
          if (item.version && item.comparator) {
            return `${item.name}${item.comparator}${item.version}`;
          }
          if (item.comment) {
            return `${item.name}: ${item.comment}`;
          }
          return item.name;
        })
        .sort()
        .join('\n')}\n`;
    }
    return '';
  }
}
