import { Test } from '@nestjs/testing';
import { DataModule } from '../../data/data.module'; // TODO use global @modules path again
import { ParserService } from './parser.service';

describe('ParserService', () => {
  let service: ParserService;

  beforeEach(async () => {
    const moduleRef = await Test.createTestingModule({
      imports: [DataModule],
      providers: [ParserService],
    }).compile();

    service = moduleRef.get(ParserService);
  });

  describe('parseDependLine', () => {
    it.each([
      ['acl', { name: 'acl' }],
      ['acl ', { name: 'acl' }],
      [' acl', { name: 'acl' }],
      [' acl ', { name: 'acl' }],

      ['acl=1.0.0', { name: 'acl', comperator: '=', version: '1.0.0' }],
      ['acl<=1.0.0', { name: 'acl', comperator: '<=', version: '1.0.0' }],
      ['acl>=1.0.0', { name: 'acl', comperator: '>=', version: '1.0.0' }],
      ['acl<1.0.0', { name: 'acl', comperator: '<', version: '1.0.0' }],
      ['acl>1.0.0', { name: 'acl', comperator: '>', version: '1.0.0' }],

      ['acl = 1.0.0', { name: 'acl', comperator: '=', version: '1.0.0' }],
      ['acl <= 1.0.0', { name: 'acl', comperator: '<=', version: '1.0.0' }],
      ['acl >= 1.0.0', { name: 'acl', comperator: '>=', version: '1.0.0' }],
      ['acl < 1.0.0', { name: 'acl', comperator: '<', version: '1.0.0' }],
      ['acl > 1.0.0', { name: 'acl', comperator: '>', version: '1.0.0' }],
    ])('should parse correctly %p', (input, expected) => {
      expect(service.parseDependLine(input)).toStrictEqual(expected);
    });
  });
});
