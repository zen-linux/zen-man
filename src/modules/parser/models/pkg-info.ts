import { PkgDependType } from '@modules/repo/constants';

export interface PkgInfo {
  name: string;
  base: string;
  groups: string[];
  desc: string;
  url: string;
  license: string;
  ver: string;
  rel: number;
  arch: string;
  sizeC: number;
  sizeI: number;
  md5: string;
  sha256: string;
  packager: string;
  buildDate: Date;
  backups: string[];
  depends: PkgInfoDepend[];
  makeDepends: PkgInfoDepend[];
  checkDepends: PkgInfoDepend[];
  optDepends: PkgInfoDepend[];
  provides: PkgInfoDepend[];
  replaces: PkgInfoDepend[];
  conflicts: PkgInfoDepend[];
}

export interface PkgInfoDepend {
  type: PkgDependType;
  name: string;
  comperator?: string;
  version?: string;
  comment?: string;
}
