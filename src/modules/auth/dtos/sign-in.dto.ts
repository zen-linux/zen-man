import { IsEmail, IsNotEmpty, IsString } from 'class-validator';

export class SignInInput {
  @IsEmail()
  @IsNotEmpty()
  email!: string;

  @IsString()
  @IsNotEmpty()
  password!: string;
}

export class SignInOutput {
  message!: string;
}
