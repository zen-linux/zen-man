import { UniqueEmailValidator } from '@modules/user/validators/unique-email.validator';
import {
  IsAlphanumeric,
  IsEmail,
  IsNotEmpty,
  IsString,
  MaxLength,
  MinLength,
  Validate,
} from 'class-validator';

export class SignUpInput {
  @IsEmail()
  @IsNotEmpty()
  @Validate(UniqueEmailValidator)
  email!: string;

  @IsString()
  @IsNotEmpty()
  password!: string;

  @IsString()
  @IsNotEmpty()
  @MaxLength(16)
  @MinLength(3)
  @IsAlphanumeric()
  username!: string;
}

export class SignUpOutput {
  message!: string;
}
