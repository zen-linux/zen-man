import { SettingsService } from '@core/abstracts/settings.service';
import { Injectable, Logger } from '@nestjs/common';
import { PassportStrategy } from '@nestjs/passport';
import { Request } from 'express';
import { Strategy } from 'passport-jwt';

export const ACCESS_TOKEN_COOKIE = 'at';

function extractJwtFromCookie(req: Request): string | null {
  const accessToken = req.cookies[ACCESS_TOKEN_COOKIE];
  if (accessToken) {
    return accessToken;
  }
  return null;
}

@Injectable()
export class JwtStrategy extends PassportStrategy(Strategy) {
  private readonly logger = new Logger(JwtStrategy.name);

  static strategy = 'jwt';

  constructor(readonly settings: SettingsService) {
    super({
      jwtFromRequest: extractJwtFromCookie,
      secretOrKey: settings.getJwtSecret(),
      ignoreExpiration: false,
    });
    this.logger.log('instantiated');
  }

  validate(payload: JwtPayload): Express.User {
    return { id: payload.sub };
  }
}
