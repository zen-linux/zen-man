import { AuthDataService } from '@modules/data/services/auth-data.service';
import { Injectable, Logger, UnauthorizedException } from '@nestjs/common';
import { PassportStrategy } from '@nestjs/passport';
import Strategy from 'passport-headerapikey';

const API_KEY_HEADER = 'X-API-KEY';

@Injectable()
export class HeaderApiKeyStrategy extends PassportStrategy(Strategy) {
  private readonly logger = new Logger(HeaderApiKeyStrategy.name);

  static strategy = 'headerapikey';

  constructor(private readonly authDataService: AuthDataService) {
    super({ header: API_KEY_HEADER, prefix: '' }, false);
    this.logger.log('instantiated');
  }

  async validate(apiKey: string): Promise<Express.User> {
    const key = await this.authDataService.getApiKeyByKey(apiKey);

    if (key === null) {
      this.logger.log('api key not found in database');
      throw new UnauthorizedException('invalid api key');
    }

    return { id: key.userId };
  }
}
