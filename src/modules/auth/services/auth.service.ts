import { AuthDataService } from '@modules/data/services/auth-data.service';
import { GroupDataService } from '@modules/data/services/group-data.service';
import { MemberDataService } from '@modules/data/services/member-data.service';
import { UserDataService } from '@modules/data/services/user-data.service';
import { Injectable, UnauthorizedException } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { compare, hash } from 'bcrypt';
import { SignInInput } from '../dtos/sign-in.dto';
import { SignUpInput } from '../dtos/sign-up.dto';

@Injectable()
export class AuthService {
  constructor(
    private readonly authDataService: AuthDataService,
    private readonly userDataService: UserDataService,
    private readonly groupDataService: GroupDataService,
    private readonly memberDataService: MemberDataService,
    private readonly jwtService: JwtService,
  ) {}

  async signUp(signUpDto: SignUpInput): Promise<void> {
    const user = await this.userDataService.createUser({
      email: signUpDto.email,
      password: await hash(signUpDto.password, 10),
      name: signUpDto.username,
    });

    const group = await this.groupDataService.createGroup({
      name: user.name,
      description: `Primary group of ${user.name}`,
    });

    await this.memberDataService.createMember({
      userId: user.id,
      groupId: group.id,
      isAdmin: true,
    });

    /*const sentMailInfo = await this.mailService.send({
      to: user.email,
      subject: 'User created',
      text: 'Your user was created',
    });
    //console.log('message id:', sentMailInfo.id);*/
  }

  async signIn(data: SignInInput): Promise<{ accessToken: string }> {
    const user = await this.authDataService.getUserByEmail(data.email);
    if (!user) {
      throw new UnauthorizedException();
    }
    const passwordMatch = await compare(data.password, user.password);
    if (!passwordMatch) {
      throw new UnauthorizedException();
    }

    const payload: JwtPayload = { sub: user.id };
    const accessToken = await this.jwtService.sign(payload);

    return { accessToken };
  }
}
