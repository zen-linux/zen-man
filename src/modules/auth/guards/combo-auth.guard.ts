import { ExecutionContext, Injectable, Logger } from '@nestjs/common';
import { Reflector } from '@nestjs/core';
import { AuthGuard } from '@nestjs/passport';
import { Observable } from 'rxjs';
import { IS_PUBLIC_KEY } from '../decorators/public.decorator';
import { HeaderApiKeyStrategy } from '../passport-strategies/header-api-key.strategy';
import { JwtStrategy } from '../passport-strategies/jwt.strategy';

@Injectable()
export class ComboAuthGuard extends AuthGuard([
  JwtStrategy.strategy,
  HeaderApiKeyStrategy.strategy,
]) {
  private readonly logger = new Logger(ComboAuthGuard.name);

  constructor(private readonly reflector: Reflector) {
    super();
  }

  canActivate(
    context: ExecutionContext,
  ): boolean | Promise<boolean> | Observable<boolean> {
    //this.logger.log('canActivate');

    const isPublic = this.reflector.getAllAndOverride<boolean>(IS_PUBLIC_KEY, [
      context.getHandler(),
      context.getClass(),
    ]);

    if (isPublic) {
      this.logger.log('skipping auth check because public flag is set');
      return true;
    }

    return super.canActivate(context);
  }
}
