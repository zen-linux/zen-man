import { SettingsService } from '@core/abstracts/settings.service';
import { GroupModule } from '@modules/group/group.module';
import { MemberModule } from '@modules/member/member.module';
import { SettingsModule } from '@modules/settings/settings.module';
import { UserModule } from '@modules/user/user.module';
import { Module } from '@nestjs/common';
import { APP_GUARD } from '@nestjs/core';
import { JwtModule } from '@nestjs/jwt';
import { AuthController } from './controllers/auth.controller';
import { ComboAuthGuard } from './guards/combo-auth.guard';
import { HeaderApiKeyStrategy } from './passport-strategies/header-api-key.strategy';
import { JwtStrategy } from './passport-strategies/jwt.strategy';
import { AuthService } from './services/auth.service';

@Module({
  imports: [
    SettingsModule,
    UserModule,
    GroupModule,
    MemberModule,
    JwtModule.registerAsync({
      imports: [SettingsModule],
      inject: [SettingsService],
      useFactory: (settings: SettingsService) => ({
        secret: settings.getJwtSecret(),
        signOptions: { expiresIn: '1h' },
      }),
    }),
  ],
  providers: [
    HeaderApiKeyStrategy,
    AuthService,
    JwtStrategy,
    {
      provide: APP_GUARD,
      useClass: ComboAuthGuard,
    },
  ],
  controllers: [AuthController],
})
export class AuthModule {}
