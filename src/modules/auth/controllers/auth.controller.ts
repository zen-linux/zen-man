import { Public } from '@modules/auth/decorators/public.decorator';
import { AuthService } from '@modules/auth/services/auth.service';
import { Body, Controller, HttpCode, Post, Res } from '@nestjs/common';
import { CookieOptions, Response } from 'express';
import { API_PREFIX } from '../../../main';
import { SignInInput } from '../dtos/sign-in.dto';
import { SignUpInput } from '../dtos/sign-up.dto';

@Controller({
  path: 'auth',
  version: '1',
})
export class AuthController {
  constructor(private readonly authService: AuthService) {}

  @Public()
  @HttpCode(201)
  @Post('signup')
  async signUp(@Body() signUpDto: SignUpInput): Promise<void> {
    await this.authService.signUp(signUpDto);
  }

  @Public()
  @HttpCode(200)
  @Post('signin')
  async signIn(
    @Body() signInDto: SignInInput,
    @Res({ passthrough: true }) res: Response,
  ): Promise<void> {
    const tokens = await this.authService.signIn(signInDto);
    res.cookie('at', tokens.accessToken, this.getCookieOptions(true));
    /*res.cookie('rt', tokens.refreshToken, {
      httpOnly: true,
      sameSite: 'strict',
      maxAge: 1000 * 60 * 60 * 24 * 365, // 1 year
      path: req.originalUrl.replace('signin', 'refresh'),
    });*/
  }

  @HttpCode(200)
  @Post('signout')
  async signOut(@Res({ passthrough: true }) res: Response): Promise<void> {
    res.clearCookie('at', this.getCookieOptions());
  }

  private getCookieOptions(includeMaxAge = false): CookieOptions {
    return {
      httpOnly: true,
      sameSite: 'strict',
      path: `/${API_PREFIX}`,
      maxAge: includeMaxAge ? 1000 * 60 * 60 * 24 * 365 : undefined,
    };
  }
}
