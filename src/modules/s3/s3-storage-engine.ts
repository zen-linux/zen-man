import { DeleteObjectCommand, S3Client } from '@aws-sdk/client-s3';
import { Upload } from '@aws-sdk/lib-storage';
import { MulterFileAddons } from '@core/models/multer-file';
import { createHash } from 'crypto';
import { Request } from 'express';
import * as multer from 'multer';
import { v4 as uuid } from 'uuid';

export function s3StorageEngine(
  options: s3StorageEngineOptions,
): S3StorageEngine {
  return new S3StorageEngine(options);
}

interface s3StorageEngineOptions {
  s3Client: S3Client;
  bucket: string;
  dest?: string;
  public?: boolean;
  metaData?: Record<string, string>;
}

class S3StorageEngine implements multer.StorageEngine {
  constructor(private readonly options: s3StorageEngineOptions) {}

  _handleFile(
    _: Request,
    file: Express.Multer.File,
    callback: (
      error?: unknown,
      info?: MulterFileAddons & Partial<Express.Multer.File>,
    ) => void,
  ): void {
    console.log('_handleFile file:', file);
    Promise.all([
      new Promise<string>((resolve, reject) => {
        file.stream
          .pipe(createHash('md5').setEncoding('hex'))
          .on('data', (md5) => {
            console.log('md5 calculation done');
            resolve(md5);
          })
          .on('error', (error) => reject(error));
      }),
      new Promise<string>((resolve, reject) => {
        file.stream
          .pipe(createHash('sha256').setEncoding('hex'))
          .on('data', (sha256) => {
            console.log('sha256 calculation done');
            resolve(sha256);
          })
          .on('error', (error) => reject(error));
      }),
      new Promise<{ fileName: string; path: string }>((resolve, reject) => {
        //const id = randomBytes(20).toString('hex');
        const id = uuid();
        const key = `${
          this.options.dest ? this.options.dest : '_uploads'
        }/${id}`;

        /*file.stream.on('data', (data: Buffer) => {
          console.log('file stream data', data.length);
        });*/

        new Upload({
          client: this.options.s3Client,
          params: {
            Bucket: this.options.bucket,
            Key: key,
            ACL: this.options.public ? 'public-read' : 'private',
            Body: file.stream,
            ContentType: file.mimetype,
            Metadata: this.options.metaData,
          },
        })
          .done()
          .then(() => {
            console.log('s3 request finished');
            resolve({ fileName: id, path: key });
          })
          .catch((error) => {
            //console.log('s3 error:', error);
            reject(error);
          });
      }),
    ])
      .then(([md5, sha256, s3Info]) => {
        console.log('storage engine finished');
        callback(null, {
          filename: s3Info.fileName,
          path: s3Info.path,
          md5,
          sha256,
        });
      })
      .catch((error) => {
        callback(error);
      });
  }

  _removeFile(
    _: Request,
    file: Express.Multer.File,
    callback: (error: Error | null) => void,
  ): void {
    //console.log('_removeFile file:', file);
    this.options.s3Client
      .send(
        new DeleteObjectCommand({
          Bucket: this.options.bucket,
          Key: file.path,
        }),
      )
      .then(() => callback(null))
      .catch((error) => callback(error));
  }
}
