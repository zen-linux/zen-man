import { SettingsModule } from '@modules/settings/settings.module';
import { Module } from '@nestjs/common';
import { S3Service } from './services/s3.service';

@Module({
  imports: [SettingsModule],
  providers: [S3Service],
  exports: [S3Service],
})
export class S3Module {}
