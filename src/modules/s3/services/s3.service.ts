import {
  CopyObjectCommand,
  DeleteObjectCommand,
  DeleteObjectsCommand,
  GetObjectCommand,
  HeadObjectCommand,
  HeadObjectCommandOutput,
  ListObjectsCommand,
  ListObjectsCommandOutput,
  NotFound,
  PutObjectCommand,
  S3Client,
} from '@aws-sdk/client-s3';
import { SettingsService } from '@core/abstracts/settings.service';
import { StorageItem } from '@core/models/storage-item';
import { createS3ReadStream, createS3WriteStream } from '@masterfix/s3-streams';
import { Injectable } from '@nestjs/common';
import { extname } from 'path';
import { Readable, Writable } from 'stream';

@Injectable()
export class S3Service {
  readonly client: S3Client;
  readonly assetsBucket: string;
  readonly reposBucket: string;

  constructor(settingsService: SettingsService) {
    this.client = new S3Client({
      endpoint: settingsService.getS3Endpoint(),
      region: settingsService.getS3Region(),
      credentials: {
        accessKeyId: settingsService.getS3AccessKey(),
        secretAccessKey: settingsService.getS3SecrectKey(),
      },
    });
    this.assetsBucket = settingsService.getS3AssetsBucket();
    this.reposBucket = settingsService.getS3ReposBucket();
  }

  async mkdir(path: string, options: { bucket: string }): Promise<void> {
    const dirPath = path.endsWith('/') ? path : `${path}/`;

    for (const pathPart of dirPath.split('/')) {
      const dir = `${pathPart}/`;

      await this.client.send(
        new HeadObjectCommand({
          Bucket: options.bucket,
          Key: dir,
        }),
      );
    }

    this.client.send(
      new ListObjectsCommand({
        Bucket: options.bucket,
      }),
    );

    this.client.send(
      new ListObjectsCommand({
        Bucket: options.bucket,
      }),
    );

    await this.client.send(
      new PutObjectCommand({
        Bucket: options.bucket,
        Key: dirPath,
      }),
    );
  }

  async mv(
    srcPath: string,
    destPath: string,
    options: {
      bucket: string;
      public?: boolean;
      metaData?: Record<string, string>;
    },
  ): Promise<void> {
    await this.client.send(
      new CopyObjectCommand({
        CopySource: `${options.bucket}/${srcPath}`,
        Bucket: options.bucket,
        Key: destPath,
        ACL: options?.public ? 'public-read' : undefined,
        Metadata: options?.metaData,
      }),
    );

    await this.rm(srcPath, { bucket: options.bucket });
  }

  async rm(path: string, options: { bucket: string }): Promise<void> {
    if (path.endsWith('/')) {
      // delete folder and content
      const objectsResponse = await this.client.send(
        new ListObjectsCommand({
          Bucket: options.bucket,
          Prefix: path,
        }),
      );

      if (objectsResponse.Contents && objectsResponse.Contents.length > 0) {
        await this.client.send(
          new DeleteObjectsCommand({
            Bucket: options.bucket,
            Delete: {
              Objects: objectsResponse.Contents.map((obj) => ({
                Key: obj.Key,
              })),
            },
          }),
        );
      }
    } else {
      // delete single object
      await this.client.send(
        new DeleteObjectCommand({
          Bucket: options.bucket,
          Key: path,
        }),
      );
    }
  }

  async put(
    path: string,
    srcStream: Readable,
    options: {
      bucket: string;
      metaData?: Record<string, string>;
    },
  ): Promise<void> {
    return new Promise<void>((resolve, reject) => {
      let mime: string | undefined;
      switch (extname(path)) {
        case '.html':
          mime = 'text/html';
          break;
        case '.css':
          mime = 'text/css';
          break;
        case '.ico':
          mime = 'image/x-icon';
          break;
        default:
          mime = 'application/octet-stream';
      }

      const writeStream = createS3WriteStream({
        s3Client: this.client,
        bucket: options.bucket,
        key: path,
        public: true,
        mime: mime,
        metaData: options.metaData,
      })
        .on('finish', () => {
          resolve();
        })
        .on('error', (error) => {
          reject(error);
        });

      if (srcStream) {
        srcStream.pipe(writeStream);
      } else {
        writeStream.end('');
      }
    });
  }

  // TODO deprecate this
  async getReadableStream(
    path: string,
    options: { bucket: string },
  ): Promise<Readable> {
    return createS3ReadStream({
      bucket: options.bucket,
      s3Client: this.client,
      key: path,
    });
  }

  async getReadable(
    path: string,
    options: { bucket: string },
  ): Promise<Readable> {
    const response = await this.client.send(
      new GetObjectCommand({
        Bucket: options.bucket,
        Key: path,
      }),
    );
    if (!(response.Body instanceof Readable)) {
      throw new Error('body is not readable');
    }
    return response.Body;
  }

  async get(
    path: string,
    destStream: Writable,
    options: { bucket: string },
  ): Promise<void> {
    const response = await this.client.send(
      new GetObjectCommand({
        Bucket: options.bucket,
        Key: path,
      }),
    );

    await new Promise<void>((resolve, reject) => {
      if (response.Body instanceof Readable) {
        response.Body.pipe(destStream)
          .on('error', (err) => reject(err))
          .on('close', () => resolve());
      } else {
        reject(new Error('response body is not a readable'));
      }
    });
  }

  async ls(path: string, options: { bucket: string }): Promise<StorageItem[]> {
    const storageItems: StorageItem[] = [];

    let isTruncated = true;
    let marker: string | undefined = undefined;

    while (isTruncated) {
      const objectsResponse: ListObjectsCommandOutput = await this.client.send(
        new ListObjectsCommand({
          Bucket: options.bucket,
          Prefix: `${path}/`,
          Marker: marker,
        }),
      );

      if (!objectsResponse.Contents) {
        break;
      }

      isTruncated = !!objectsResponse.IsTruncated;

      if (isTruncated) {
        marker = objectsResponse.Contents.slice(-1)[0].Key;
      }

      for (const object of objectsResponse.Contents) {
        if (object.Key === undefined) {
          continue;
        }

        if (object.Size === undefined) {
          throw new Error('Size is not set');
        }
        if (object.ETag === undefined) {
          throw new Error('ETag is not set');
        }
        if (object.LastModified === undefined) {
          throw new Error('LastModified is not set');
        }

        storageItems.push({
          path: object.Key,
          size: object.Size,
          date: object.LastModified,
          etag: object.ETag,
        });
      }
    }

    return storageItems;
  }

  async stat(
    path: string,
    options: { bucket: string },
  ): Promise<StorageItem | null> {
    let result: HeadObjectCommandOutput;
    try {
      result = await this.client.send(
        new HeadObjectCommand({
          Bucket: options.bucket,
          Key: path,
        }),
      );
    } catch (error) {
      if (error instanceof NotFound) {
        return null;
      }
      throw error;
    }

    if (result.ContentLength === undefined) {
      throw new Error('ContentLength is not set');
    }
    if (result.ETag === undefined) {
      throw new Error('ETag is not set');
    }
    if (result.LastModified === undefined) {
      throw new Error('LastModified is not set');
    }

    const storageItem: StorageItem = {
      path,
      size: result.ContentLength,
      etag: result.ETag,
      date: result.LastModified,
    };

    return storageItem;
  }
}
