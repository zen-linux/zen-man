FROM docker.io/library/node:22 as builder
ARG TARGETARCH
RUN apt-get update && apt-get install -y wget
COPY misc/scripts/download-dumb-init.sh .
RUN sh download-dumb-init.sh /usr/bin/dumb-init
RUN npm config set update-notifier false
WORKDIR /app
COPY package*.json ./
RUN npm install --omit=dev
COPY prisma .
RUN npx prisma generate --no-hints
RUN rm -v package*.json

FROM docker.io/library/node:22
WORKDIR /app
COPY --from=builder /app /app/
COPY dist .
#COPY apps/api/prisma .
RUN useradd -m zen
RUN chown -R zen:zen .
USER zen
EXPOSE 3000
COPY --from=builder /usr/bin/dumb-init /usr/bin/dumb-init
ENV NODE_OPTIONS="--dns-result-order=ipv4first"
RUN npm config set update-notifier false
CMD ["dumb-init", "sh", "-c", "npx prisma db push && node main.js"]
